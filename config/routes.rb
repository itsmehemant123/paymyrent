require 'api_constraints'

Rails.application.routes.draw do

  #use this for production - api.paymyrent.in instead of localhost - see constrains
  #namespace :api,   defaults: { format: :json }, constraints: { subdomain: 'api' }, path: '/' do

  namespace :api,   defaults: { format: :json }, path: '/v1/' do #versioning

  #namespace :api,   defaults: { format: :json }, path: '/' do
    scope module: :v1,
          constraints: ApiConstraints.new(version: 1, default: true) do
      # We are going to list our resources here
      resources :users, only: [:show, :create, :update, :destroy]
      resources :sessions, only: [:create, :destroy]
      resources :profiles, only: [:create]
      match 'profiles', to: 'profiles#show', via: :get
      match 'profiles', to: 'profiles#update', via: [:put, :patch]

      get 'profiles/:id' => 'profiles#show'
      put 'profiles/:id' => 'profiles#update'

      resources :rooms, only: [:create, :update, :destroy]

      get '/rooms' => 'rooms#show'

      #search rooms and profiles
      get '/rooms/search' => 'rooms#search'
      get '/profiles/search' => 'profiles#search'

      #adding room mates
      get '/rooms/:id/roommates/show' => 'roommates#show'
      post '/rooms/:id/roommates/add' => 'roommates#add'
      post '/rooms/:id/roommates/remove' => 'roommates#remove'
      post '/rooms/:id/roommates/join' => 'roommates#join'
      post '/rooms/:id/roommates/leave' => 'roommates#leave'

      #handle actions
      post '/actions/:id/:verb' => 'actions#do_action'

      #get notifications
      get '/notifications' => 'notifications#show'

=begin
      #add the devise model for admins first
      #mount the resque monitoring server somewhere
      devise_for :admin_users, ActiveAdmin::Devise.config
      authenticate :admin_user do #replace admin_user(s) with whatever model your users are stored in.
        mount Resque::Server.new, :at => "/jobs"
      end

=end

    end
  end

  devise_for :users

  unless Rails.application.config.consider_all_requests_local # so that in development you would see real exceptions, but not in production.
    #Rails 4 doesn't allow ApplicationController to rescue_from on routing errors, so do this -
    match '*unmatched_route', :to => 'application#raise_not_found!', :via => :all
  end
end
