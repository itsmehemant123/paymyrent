# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150902115852) do

  create_table "actions", force: :cascade do |t|
    t.integer  "from",       limit: 4
    t.string   "action",     limit: 255
    t.integer  "target",     limit: 4
    t.integer  "by",         limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "current_transactions", force: :cascade do |t|
    t.integer  "user_id",        limit: 4
    t.integer  "room_id",        limit: 4
    t.datetime "pay_period"
    t.decimal  "amount",                     precision: 16, scale: 2
    t.string   "sent_props",     limit: 255
    t.string   "received_props", limit: 255
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
  end

  create_table "invites", force: :cascade do |t|
    t.string   "email",      limit: 255
    t.integer  "room_id",    limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "people", force: :cascade do |t|
    t.string   "first_name",                   limit: 255, default: ""
    t.string   "last_name",                    limit: 255, default: ""
    t.string   "middle_name",                  limit: 255, default: ""
    t.string   "address",                      limit: 255, default: ""
    t.string   "phone",                        limit: 255, default: ""
    t.string   "bank_account",                 limit: 255, default: ""
    t.integer  "admin_room_id",                limit: 4
    t.integer  "room_id",                      limit: 4
    t.integer  "user_id",                      limit: 4
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
    t.string   "display_picture_file_name",    limit: 255
    t.string   "display_picture_content_type", limit: 255
    t.integer  "display_picture_file_size",    limit: 4
    t.datetime "display_picture_updated_at"
  end

  add_index "people", ["user_id"], name: "index_people_on_user_id", using: :btree

  create_table "rentings", force: :cascade do |t|
    t.integer  "room_id",    limit: 4
    t.integer  "person_id",  limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "rentings", ["person_id"], name: "index_rentings_on_person_id", using: :btree
  add_index "rentings", ["room_id"], name: "index_rentings_on_room_id", using: :btree

  create_table "rooms", force: :cascade do |t|
    t.decimal  "rent",                           precision: 16, scale: 2
    t.string   "room_name",          limit: 255
    t.string   "owner_first_name",   limit: 255
    t.string   "owner_last_name",    limit: 255
    t.string   "owner_bank_account", limit: 255
    t.decimal  "rent_left",                      precision: 16, scale: 2
    t.datetime "pay_period_start"
    t.string   "pay_period_type",    limit: 255
    t.datetime "created_at",                                              null: false
    t.datetime "updated_at",                                              null: false
  end

  create_table "transactions_audits", force: :cascade do |t|
    t.integer  "user_id",        limit: 4
    t.integer  "room_id",        limit: 4
    t.datetime "pay_period"
    t.decimal  "amount",                     precision: 16, scale: 2
    t.string   "sent_props",     limit: 255
    t.string   "received_props", limit: 255
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "auth_token",             limit: 255
  end

  add_index "users", ["auth_token"], name: "index_users_on_auth_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "rentings", "people"
  add_foreign_key "rentings", "rooms"
end
