class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|
      t.decimal :rent
      t.string :room_name
      t.string :owner_first_name
      t.string :owner_last_name
      t.string :owner_bank_account
      t.decimal :rent_left
      t.datetime :pay_period_start
      t.string :pay_period_type

      t.timestamps null: false
    end
  end
end
