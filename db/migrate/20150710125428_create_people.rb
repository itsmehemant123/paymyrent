class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.string :first_name, default: ""
      t.string :last_name, default: ""
      t.string :middle_name, default: ""
      t.string :address, default: ""
      t.string :phone, default: ""
      t.string :bank_account, default: ""
      t.string :photo_location, default: ""
      t.integer :admin_room_id
      t.integer :room_id
      t.integer :user_id

      t.timestamps null: false
    end
    add_index :people, :user_id
  end
end
