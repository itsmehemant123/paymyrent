class RemovePhotoLocationFromPeople < ActiveRecord::Migration
  def change
    remove_column :people, :photo_location, :string
  end
end
