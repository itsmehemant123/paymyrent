class AddPaperclipToPeople < ActiveRecord::Migration
  def change
    add_attachment :people, :display_picture
  end
end
