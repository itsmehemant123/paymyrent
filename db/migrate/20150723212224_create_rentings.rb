class CreateRentings < ActiveRecord::Migration
  def change
    create_table :rentings do |t|
      t.references :room, index: true, foreign_key: true
      t.references :person, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
