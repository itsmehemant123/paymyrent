class CreateTransactionsAudits < ActiveRecord::Migration
  def change
    create_table :transactions_audits do |t|
      t.integer :user_id
      t.integer :room_id
      t.datetime :pay_period
      t.decimal :amount, precision: 16, scale: 2
      t.string :sent_props
      t.string :received_props

      t.timestamps null: false
    end
  end
end
