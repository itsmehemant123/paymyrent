class CreateActions < ActiveRecord::Migration
  def change
    create_table :actions do |t|
      t.integer :from
      t.string :action
      t.integer :target
      t.integer :by

      t.timestamps null: false
    end
  end
end
