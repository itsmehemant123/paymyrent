class ChangeDateFormatInRoomsTable < ActiveRecord::Migration
  def change
    change_column :rooms, :rent, :decimal, :precision => 16, :scale => 2
    change_column :rooms, :rent_left, :decimal, :precision => 16, :scale => 2
  end
end
