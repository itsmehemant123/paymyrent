module Handlers
  class InvitesHandler
    def self.SendInvite(email_id, room_id)
      #send email, add to invites
      invite = Invite.create(email: email_id, room_id: room_id)
      #send email
      InviteMailer.send_invite(invite).deliver_now
      return invite.id
    end
  end
end