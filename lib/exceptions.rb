module Exceptions

  class InvalidEmailException < StandardError; end

  #Notifications/Actions
  class MultipleUserActionsException < StandardError; end
  class NoUserActionException < StandardError; end
  class NotUserActionException < StandardError; end
  class NoActionException < StandardError; end

  #actions controller
  class ActionUserMismatchException < StandardError; end

  #rooms controller
  class RoomExistsInProfileException < StandardError; end
  class RoomParamsMissingException < StandardError
    attr_accessor :validation_errors
    def initialize(errors)
      @validation_errors = errors
    end
  end
  class RoomAlreadyExistsException < StandardError; end
  class RoomUserNotAdminOrMateException < StandardError; end
  class RoomDoesntExistException < StandardError; end
  class RoomHasNoAdminException < StandardError; end
  class UserNotInRoomException < StandardError; end
  class NoSearchParamsException < StandardError; end
  class CannotRemoveAdminFromRoomException < StandardError; end
  class CannotDeleteRoomException < StandardError; end
  class CannotLeaveRoomException < StandardError; end

  #roommates controller
  class RoomMateHasRoomException < StandardError; end

  #paymenthandler
  class RestClientException < StandardError; end

end