# resque.rake
require 'resque/tasks'
require 'resque/scheduler/tasks'

task "resque:setup" => :environment do
  # Place One time Job Over Here
  # Example
  # Resque.enqueue_in(10.seconds, OrderCharge, :user_id => 1).

  # Commenting the next line requires adding the queue attribute while running the worker
  ENV['QUEUE'] = '*'

=begin

  Start the scheduler: $ rake resque:scheduler

  Every day you should see a new job get added to the queue
  2012-02-17 16:19:59 queueing DailyPaymentJob (every_day)
  Start the worker: $ rake resque:work

  Every day the job runs
=end

end