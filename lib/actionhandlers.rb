module ActionHandlers

  require 'exceptions'
  module ActionTypes
    SHOULD_ROOMDELETE ||= 0
    INFO_INVITEUSERJOINED ||= 1
    SHOULD_USERJOINROOMREQUEST ||= 2
    INFO_ROOMJOINREQUESTACCEPTED ||= 3
    INFO_ROOMJOINREQUESTREJECTED ||= 4
    INFO_ROOMJOININVITESENT ||= 5
    INFO_USERADDEDTOROOM ||= 6
    INFO_USERREMOVEDFROMROOM ||= 7
    INFO_ROOMJOINREQUESTSENT ||= 8
    INFO_USERJOINEDROOM ||= 9
    INFO_USERLEFTROOM ||= 10
    INFO_ROOMUPDATED ||= 11
    INFO_USERISROOMADMIN ||= 12
    INFO_ROOMRENTPAID ||= 13
    INFO_ROOMRENTPROCESSED ||= 14
    INFO_ROOMRENTNOTPAID ||= 15
    INFO_ROOMRENTPENDINGPERIOD ||=16

    class ActionClassHandler
      def self.get_action_object(type, action)
        case type.to_i
          when SHOULD_ROOMDELETE
          when INFO_INVITEUSERJOINED.to_i
            return InvitedUserJoinedAction.new(action)
          when SHOULD_USERJOINROOMREQUEST.to_i
            return UserJoinRoomRequestAction.new(action)
          when INFO_ROOMJOINREQUESTACCEPTED.to_i
            return RoomJoinRequestAcceptAction.new(action)
          when INFO_ROOMJOINREQUESTREJECTED.to_i
            return RoomJoinRequestRejectAction.new(action)
          when INFO_ROOMJOININVITESENT.to_i
            return InviteEmailToRoomAction.new(action)
          when INFO_USERADDEDTOROOM.to_i
            return UserAddedToRoomAction.new(action)
          when INFO_USERREMOVEDFROMROOM.to_i
            return UserRemovedFromRoomAction.new(action)
          when INFO_ROOMJOINREQUESTSENT.to_i
            return RoomJoinRequestSentAction.new(action)
          when INFO_USERJOINEDROOM.to_i
            return UserJoinedRoomAction.new(action)
          when INFO_USERLEFTROOM.to_i
            return UserLeftRoomAction.new(action)
          when INFO_ROOMUPDATED.to_i
            return RoomUpdatedAction.new(action)
          when INFO_USERISROOMADMIN.to_i
            return UserIsRoomAdminAction.new(action)
          when INFO_ROOMRENTPAID.to_i
            return RoomRentPaidAction.new(action)
          when INFO_ROOMRENTPROCESSED.to_id
            return RoomRentUserAmountPaidAction.new(action)
          when INFO_ROOMRENTNOTPAID.to_i
            return RoomRentNotPaidByUserAction.new(action)
          when INFO_ROOMRENTPENDINGPERIOD.to_i
            return UserRentPaymentPendingPeriodAction.new(action)
          else
            puts("ERROR - #{type.to_i}")
            return NoAction.new
        end
      end
    end
  end

  class NoAction

  end

  class BaseAction
    @action
    @from_user
    @by_user
    @target_object

    def initialize(action)
      @action= action
    end

    def get_action

    end

    def clear_action

    end
  end

  class InfoAction < BaseAction
    def initialize(action)
      super(action)
    end

    def get_action_and_destroy

    end

  end

  class UserAction < BaseAction
    def initialize(action)
      super(action)
    end

    def do_action

    end
  end

  #Individual Action Handles - extending either UserAction or Info Action

  class NotificationActionHandle
    @action
    def addNotification(from:, by:, target:, type:)
      #save the action
      Action.create(from: from, by: by, target: target, action: type)
    end

    def is_multiple?
      if @action.nil?
        return false
      elsif @action.count() <= 1
        return false
      else
        return true
      end
    end

    def load_action(by:, from: 'none', target: 'none', type: 'none')
      #load the action/actions
      @action = Action.where(by: by)

      unless type == 'none'
        @action = @action.where(type: type)
      end

      unless target == 'none'
        @action = @action.where(target: target)
      end

      #is this even required?
      unless from == 'none'
        @action = @action.where(from: from)
      end

      #raise_exception_if_action_not_loaded
    end

    def load_action_by_id(id)
      @action = Action.where(id: id)

      raise_exception_if_action_not_loaded
    end

    def get_raw
      @action
    end

    def get_action!
      #do for each - throw them in a loop
      #looping
      serialized_notifications = @action.map do |item|
        obj = ActionTypes::ActionClassHandler.get_action_object(item.action, item)
        if obj.respond_to?(:get_action_and_destroy)
          #only info actions
          obj.get_action_and_destroy
        else
          obj.get_action
        end
      end

      serialized_notifications
    end

    def get_action
      #do for each - throw them in a loop
      serialized_notifications = @action.map do |item|
        ActionTypes::ActionClassHandler.get_action_object(item.action, item).get_action
      end

      serialized_notifications
    end

    def clear_action
      #do for each - throw them in a loop
      @action.map do |item|
        item.destroy
      end
    end

    def do_action(params)
      #load action by id
      raise Exceptions::NoUserActionException.new('No User action loaded') if @action.count() == 0
      raise Exceptions::MultipleUserActionsException.new('Single Action must be loaded') if @action.count() > 1

      action = @action.first()
      obj = ActionTypes::ActionClassHandler.get_action_object(action.action, action)

      raise Exceptions::NotUserActionException.new('Not a user action') unless obj.respond_to?(:do_action)

      obj.do_action(params)
    end

    def do_and_clear_action(params)
      #load action by id
      raise Exceptions::NoUserActionException.new('No User action loaded') if @action.count() == 0
      raise Exceptions::MultipleUserActionsException.new('Single Action must be loaded') if @action.count() > 1

      action = @action.first()
      obj = ActionTypes::ActionClassHandler.get_action_object(action.action, action)

      raise Exceptions::NotUserActionException.new('Not a user action') unless obj.respond_to?(:do_action)

      obj.do_action(params)
      obj.clear_action
    end

    private

    def raise_exception_if_action_not_loaded
      if @action.count() == 0
        raise Exceptions::NoActionException.new('No Such action exists')
      end
    end
  end

  class InvitedUserJoinedAction < InfoAction
    def initialize(action)
      super(action)
      @from_user = User.where(id: @action.from).first
      @by_user = User.where(id: @action.by).first
      @target_object = Room.where(id: @action.target).first
    end

    def get_action
      { type: 'Info', text: "The user #{@from_user.person.first_name} (invited - #{@from_user.email}), has joined the room #{@target_object.room_name}" }
    end

    def get_action_and_destroy
      @action.destroy
      get_action
    end

    def clear_action
      @action.destroy
    end
  end

  class InviteEmailToRoomAction < InfoAction
    def initialize(action)
      super(action)
      @from_user = Invite.where(id: @action.from).first().email
      @by_user = User.where(id: @action.by).first
      @target_object = Room.where(id: @action.target).first
    end

    def get_action
      { type: 'Info', text: "An invite has been sent to #{@from_user} to join the room #{@target_object.room_name}" }
    end

    def get_action_and_destroy
      @action.destroy
      get_action
    end

    def clear_action
      @action.destroy
    end
  end

  class UserAddedToRoomAction < InfoAction
    def initialize(action)
      super(action)
      @from_user = User.where(id: @action.from).first
      @by_user = User.where(id: @action.by).first
      @target_object = Room.where(id: @action.target).first
    end

    def get_action
      { type: 'Info', text: "You've been added to the room #{@target_object.room_name} by #{@from_user.person.first_name}" }
    end

    def get_action_and_destroy
      @action.destroy
      get_action
    end

    def clear_action
      @action.destroy
    end
  end

  class UserRemovedFromRoomAction < InfoAction
    def initialize(action)
      super(action)
      @from_user = User.where(id: @action.from).first
      @by_user = User.where(id: @action.by).first
      @target_object = Room.where(id: @action.target).first
    end

    def get_action
      { type: 'Info', text: "You've been removed from the room #{@target_object.room_name} by #{@from_user.person.first_name}" }
    end

    def get_action_and_destroy
      @action.destroy
      get_action
    end

    def clear_action
      @action.destroy
    end
  end

  class RoomJoinRequestRejectAction < InfoAction
    def initialize(action)
      super(action)
      @from_user = User.where(id: @action.from).first
      @by_user = User.where(id: @action.by).first
      @target_object = Room.where(id: @action.target).first
    end

    def get_action
      { type: 'Info', text: "Your request to join room #{@target_object.room_name} has been rejected" }
    end

    def get_action_and_destroy
      @action.destroy
      get_action
    end

    def clear_action
      @action.destroy
    end
  end

  class RoomJoinRequestAcceptAction < InfoAction
    def initialize(action)
      super(action)
      @from_user = User.where(id: @action.from).first
      @by_user = User.where(id: @action.by).first
      @target_object = Room.where(id: @action.target).first
    end

    def get_action
      { type: 'Info', text: "Your request to join room #{@target_object.room_name} has been accepted" }
    end

    def get_action_and_destroy
      @action.destroy
      get_action
    end

    def clear_action
      @action.destroy
    end
  end

  class RoomJoinRequestSentAction < InfoAction
    def initialize(action)
      super(action)
      @from_user = User.where(id: @action.from).first
      @by_user = User.where(id: @action.by).first
      @target_object = Room.where(id: @action.target).first
    end

    def get_action
      { type: 'Info', text: "Your request to join room #{@target_object.room_name} has been sent to the admin #{@from_user.person.first_name}" }
    end

    def get_action_and_destroy
      @action.destroy
      get_action
    end

    def clear_action
      @action.destroy
    end
  end

  class UserJoinRoomRequestAction < UserAction

    def initialize(action)
      super(action)
      @from_user = User.where(id: @action.from).first
      @by_user = User.where(id: @action.by).first
      @target_object = Room.where(id: @action.target).first
    end

    def get_action
      { type: 'Response', text: "The user #{@from_user.person.first_name} wants to join #{@target_object.room_name}. Accept?", response_options: {YES: "/actions/#{@action.id}/YES", NO: "/actions/#{@action.id}/NO"}}
    end

    def clear_action
      @action.destroy
    end

    def do_action(params)
      notificationHandler = NotificationActionHandle.new()
      if params == 'YES'
        @target_object.persons << @from_user.person
        @target_object.save
        #send notif for from person
        notificationHandler.addNotification(from: @by_user.id, by: @from_user.id, target: @target_object.id, type: ActionTypes::INFO_ROOMJOINREQUESTACCEPTED)
        #send notif to others
        @target_object.persons.map do |person|
          notificationHandler.addNotification(from: @from_user.id, by: person.user_id, target: @target_object.id, type: ActionTypes::INFO_USERJOINEDROOM) unless person.user_id == @from_user.id
        end

        clear_action
      elsif params == 'NO'
        #send notif for to person
        notificationHandler.addNotification(from: @by_user.id, by: @from_user.id, target: @target_object.id, type: ActionTypes::INFO_ROOMJOINREQUESTREJECTED)
        clear_action
      end
    end
  end

  class UserJoinedRoomAction < InfoAction
    def initialize(action)
      super(action)
      @from_user = User.where(id: @action.from).first
      @by_user = User.where(id: @action.by).first
      @target_object = Room.where(id: @action.target).first
    end

    def get_action
      { type: 'Info', text: "User #{@from_user.person.first_name} has joined the room #{@target_object.room_name}" }
    end

    def get_action_and_destroy
      @action.destroy
      get_action
    end

    def clear_action
      @action.destroy
    end
  end

  class UserLeftRoomAction < InfoAction
    def initialize(action)
      super(action)
      @from_user = User.where(id: @action.from).first
      @by_user = User.where(id: @action.by).first
      @target_object = Room.where(id: @action.target).first
    end

    def get_action
      { type: 'Info', text: "User #{@from_user.person.first_name} has left the room #{@target_object.room_name}" }
    end

    def get_action_and_destroy
      @action.destroy
      get_action
    end

    def clear_action
      @action.destroy
    end
  end

  class RoomUpdatedAction < InfoAction
    def initialize(action)
      super(action)
      #@from_user = User.where(id: @action.from).first
      #@by_user = User.where(id: @action.by).first
      #@target_object = Room.where(id: @action.target).first
    end

    def get_action
      { type: 'Info', text: "Your current room has been updated recently" }
    end

    def get_action_and_destroy
      @action.destroy
      get_action
    end

    def clear_action
      @action.destroy
    end
  end

  class UserIsRoomAdminAction < InfoAction
    def initialize(action)
      super(action)
      @from_user = User.where(id: @action.from).first
      @by_user = User.where(id: @action.by).first
      @target_object = Room.where(id: @action.target).first
    end

    def get_action
      { type: 'Info', text: "You have become the admin of the room #{@target_object.room_name}" }
    end

    def get_action_and_destroy
      @action.destroy
      get_action
    end

    def clear_action
      @action.destroy
    end
  end

  class RoomRentPaidAction < InfoAction
    def initialize(action)
      super(action)
      @by_user = User.where(id: @action.by).first
      @target_object = Room.where(id: @action.target).first
    end

    def get_action
      { type: 'Info', text: "The rent for the room #{@target_object.room_name} has been paid to the Owner #{@target_object.owner_first_name}" }
    end

    def get_action_and_destroy
      @action.destroy
      get_action
    end

    def clear_action
      @action.destroy
    end
  end

  class RoomRentUserAmountPaidAction < InfoAction
    def initialize(action)
      super(action)
      @by_user = User.where(id: @action.by).first
      @target_object = Room.where(id: @action.target).first
    end

    def get_action
      { type: 'Info', text: "Your share for the rent for the room #{@target_object.room_name} has been processed. It will be paid to the owner once remaining room mates pay the rent." }
    end

    def get_action_and_destroy
      @action.destroy
      get_action
    end

    def clear_action
      @action.destroy
    end
  end

  class RoomRentNotPaidByUserAction < InfoAction
    def initialize(action)
      super(action)
      @by_user = User.where(id: @action.by).first
      @target_object = Room.where(id: @action.target).first
    end

    def get_action
      { type: 'Info', text: "Your are yet to pay the rent for the room #{@target_object.room_name}. Please pay the rent as soon as possible." }
    end

    def get_action_and_destroy
      @action.destroy
      get_action
    end

    def clear_action
      @action.destroy
    end
  end

  class UserRentPaymentPendingPeriodAction < InfoAction
    def initialize(action)
      super(action)
      @days = @action.from
      @by_user = User.where(id: @action.by).first
      @target_object = Room.where(id: @action.target).first
    end

    def get_action
      { type: 'Info', text: "Your are yet to pay the rent for the room #{@target_object.room_name}. Please pay the rent within the next #{@days} days." }
    end

    def get_action_and_destroy
      @action.destroy
      get_action
    end

    def clear_action
      @action.destroy
    end
  end
end