require 'spec_helper'

RSpec.describe Action, type: :model do
  before {@action = FactoryGirl.build(:action)}

  subject {@action}

  it {should respond_to(:from)}
  it {should respond_to(:action)}
  it {should respond_to(:target)}
  it {should respond_to(:by)}

  it {should validate_presence_of :from}
  it {should validate_presence_of :action}
  it {should validate_presence_of :target}
  it {should validate_presence_of :by}
end
