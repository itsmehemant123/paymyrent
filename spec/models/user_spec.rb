require 'spec_helper'

describe User do
  before {@user = FactoryGirl.build(:user)}

  subject {@user}

  it { should respond_to(:password) }
  it { should respond_to(:password_confirmation) }
  it { should respond_to(:email) }
  it {should respond_to(:auth_token)}

  it { should validate_presence_of(:email) }
  it { should validate_uniqueness_of(:email) }
  it { should validate_confirmation_of(:password) }
  it { should allow_value('example@domain.com').for(:email) }

  it {should have_one(:person)}

  #Auth token must be unique
  it {should validate_uniqueness_of(:auth_token)}

  describe "#generate authentication_token!" do
    it "generates a unique token" do
      #Devise.stub(:friendly_token).and_return("auniquetoken123")
      allow(Devise).to receive(:friendly_token).and_return("auniquetoken123")
      @user.generate_authentication_token!
      expect(@user.auth_token).to eql "auniquetoken123"
    end

    it "generates another token when one already has been taken" do
      existing_user = FactoryGirl.create(:user, auth_token: "auniquetoken123")
      @user.generate_authentication_token!
      expect(@user.auth_token).not_to eql existing_user.auth_token
    end
  end

  describe "#Person association" do
    before do
      @user.save
      @person = FactoryGirl.create :person, user: @user
    end

    it "checks association of the person and the user" do
      expect(@user.person).to eq(@person)
    end

    it "destroys the associated person object on self destruct" do
      person = @user.person
      @user.destroy
      expect {Person.where(id: person.id).first! }.to raise_exception ActiveRecord::RecordNotFound
    end
  end

  it { should be_valid }
end
