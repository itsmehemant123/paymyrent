require 'spec_helper'

RSpec.describe CurrentTransaction, type: :model do
  before {@transaction = FactoryGirl.build(:current_transaction)}

  subject {@transaction}

  it {should respond_to(:user_id)}
  it {should respond_to(:room_id)}
  it {should respond_to(:pay_period)}
  it {should respond_to(:amount)}
  it {should respond_to(:sent_props)}
  it {should respond_to(:received_props)}

  it {should validate_presence_of :user_id}
  it {should validate_presence_of :room_id}
  it {should validate_presence_of :pay_period}
  it {should validate_presence_of :amount}
end
