require 'spec_helper'

RSpec.describe Invite, type: :model do
  before {@invite = FactoryGirl.build(:invite)}

  subject {@invite}

  it {should respond_to(:room_id)}
  it {should respond_to(:email)}

  it {should validate_presence_of :room_id}
  it {should validate_presence_of :email}

  #Required for the facebook integration - see person.rb
  #it {should validate_presence_of :address}
  #it {should validate_presence_of :phone}
  #it {should validate_presence_of :bank_account}

end
