require 'spec_helper'

describe Person do
  before {@person = FactoryGirl.build(:person)}

  subject {@person}

  it {should respond_to(:first_name)}
  it {should respond_to(:last_name)}
  it {should respond_to(:address)}
  it {should respond_to(:phone)}
  it {should respond_to(:bank_account)}
  it {should respond_to(:admin_room_id)}
  it {should respond_to(:room_id)}
  it {should respond_to(:user_id)}

  it {should validate_presence_of :first_name}
  it {should validate_presence_of :last_name}
  it {should validate_presence_of :user_id}

  #Required for the facebook integration - see person.rb
  #it {should validate_presence_of :address}
  #it {should validate_presence_of :phone}
  #it {should validate_presence_of :bank_account}

  it {should belong_to :user}

  it {should have_many(:rentings)}
  it {should have_many(:rooms).through(:rentings)}

end
