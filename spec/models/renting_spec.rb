require 'spec_helper'

describe Renting do
  let(:renting) { FactoryGirl.build :renting }
  subject { renting }

  it { should respond_to :room_id }
  it { should respond_to :person_id }

  it { should belong_to :room }
  it { should belong_to :person }
end
