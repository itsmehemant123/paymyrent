require 'spec_helper'

RSpec.describe TransactionsAudit, type: :model do
  before {@audit = FactoryGirl.build(:transactions_audit)}

  subject {@audit}

  it {should respond_to(:user_id)}
  it {should respond_to(:room_id)}
  it {should respond_to(:pay_period)}
  it {should respond_to(:amount)}
  it {should respond_to(:sent_props)}
  it {should respond_to(:received_props)}

  it {should validate_presence_of :user_id}
  it {should validate_presence_of :room_id}
  it {should validate_presence_of :pay_period}
  it {should validate_presence_of :amount}
end
