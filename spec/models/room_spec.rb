require 'spec_helper'

describe Room do
  let(:room) { FactoryGirl.build :room }
  subject { room }

  it {should respond_to(:rent)}
  it {should respond_to(:room_name)}
  it {should respond_to(:owner_first_name)}
  it {should respond_to(:owner_last_name)}
  it {should respond_to(:owner_bank_account)}
  it {should respond_to(:rent_left)}
  it {should respond_to(:pay_period_start)}
  it {should respond_to(:pay_period_type)}

  it {should validate_presence_of :rent}
  it {should validate_presence_of :room_name}
  it {should validate_presence_of :owner_first_name}
  it {should validate_presence_of :owner_last_name}
  it {should validate_presence_of :owner_bank_account}
  it {should validate_presence_of :rent_left}
  it {should validate_presence_of :pay_period_start}
  it {should validate_presence_of :pay_period_type}

  it {should validate_numericality_of(:rent)}
  it {should validate_numericality_of(:rent_left)}

  it "should not allow rent left to be greater than the rent" do
    subject.rent = 9.9
    subject.rent_left = 10.0

    expect(subject.valid?).to be_falsey
  end

  it {should have_many(:rentings)}
  it {should have_many(:persons).through(:rentings)}
end
