FactoryGirl.define do
  factory :transactions_audit do
    user_id 1
    room_id 999
    pay_period Time.zone.parse(FFaker::Time.date).utc
    amount 9.99
    sent_props "MyString"
    received_props "MyString"
  end

end
