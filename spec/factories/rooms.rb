FactoryGirl.define do
  factory :room do
    rent 9.99
    room_name FFaker::Name.name
    owner_first_name FFaker::Name.first_name
    owner_last_name FFaker::Name.last_name
    owner_bank_account FFaker::Identification.ssn
    rent_left 9.99
    pay_period_start Time.zone.parse(FFaker::Time.date).utc
    pay_period_type "MyString"

    factory :room_with_person do
      transient do
        person {create :person}
      end

      after :create do |room, evaluator|
        room.persons << evaluator.person
        room.save
        #rentings_room_person = room.rentings_room_person.where(person:evaluator.person).first
        #rentings_room_person.save
      end
    end

  end

end
