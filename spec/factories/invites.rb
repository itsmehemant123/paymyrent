FactoryGirl.define do
  factory :invite do
    email FFaker::Internet.email
    room_id 1
  end

end
