FactoryGirl.define do
  factory :person do
    first_name FFaker::Name.first_name
    last_name FFaker::Name.last_name
    middle_name ""
    address FFaker::Address.street_address
    phone FFaker::PhoneNumber.phone_number
    bank_account FFaker::Identification.ssn
    admin_room_id nil
    room_id nil
    #display_picture { fixture_file_upload(Rails.root.join('spec', 'photos', 'test.png'), 'image/png', :binary) }
    user
    #display_picture "/default-user.png"
  end

end
