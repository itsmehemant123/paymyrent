require "spec_helper"

RSpec.describe InviteMailer, type: :mailer do
  include Rails.application.routes.url_helpers

  describe ".send_invite" do
    before(:each) do
      @invite = FactoryGirl.create :invite
      @invite_mailer = InviteMailer.send_invite(@invite)
    end

    it "should be set to be delivered to the user from the invite " do
      #@invite_mailer.should deliver_to(@invite.email)
      expect(@invite_mailer.to).to eql([@invite.email])
    end

    it "should be set to be send from no-reply@marketplace.com" do
      #@invite_mailer.should deliver_from('no-reply@paymyrent.in')
      expect(@invite_mailer.from).to eql(['no-reply@paymyrent.in'])
    end

=begin
    it "should contain the user's message in the mail body" do
      @invite_mailer.should have_body_text(/Invite: ##{@order.id}/)
    end
=end

    it "should have the correct subject" do
      #@invite_mailer.should have_subject(/Invited to Paymyrent/)
      expect(@invite_mailer.subject).to eql('Invited to Paymyrent')
    end

    it "should contain the user's message in the mail body" do
      #@invite_mailer.should have_body_text(/You've been invited to use Paymyrent/)
      expect(@invite_mailer.body.encoded).to match("You've been invited to use Paymyrent")
    end
  end
end
