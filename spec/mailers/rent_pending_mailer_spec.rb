require "spec_helper"

RSpec.describe RentPendingMailer, type: :mailer do
  #include Rails.application.routes.url_helpers

  describe ".send_reminder" do
    before(:each) do
      @user = FactoryGirl.create :user
      @room = FactoryGirl.create :room
      @rent_pending_mailer = RentPendingMailer.send_reminder(@user.id, @room.id, 10)
      #@invite_mailer = InviteMailer.send_invite(@invite)
    end

    it "should be set to be delivered to the user from the invite " do
      #@invite_mailer.should deliver_to(@invite.email)
      expect(@rent_pending_mailer.to).to eql([@user.email])
    end

    it "should be set to be send from no-reply@marketplace.com" do
      #@invite_mailer.should deliver_from('no-reply@paymyrent.in')
      expect(@rent_pending_mailer.from).to eql(['no-reply@paymyrent.in'])
    end

=begin
    it "should contain the user's message in the mail body" do
      @invite_mailer.should have_body_text(/Invite: ##{@order.id}/)
    end
=end

    it "should have the correct subject" do
      #@invite_mailer.should have_subject(/Invited to Paymyrent/)
      expect(@rent_pending_mailer.subject).to eql('Pending Rent Payment')
    end

    it "should contain the user's message in the mail body" do
      #@invite_mailer.should have_body_text(/You've been invited to use Paymyrent/)
      expect(@rent_pending_mailer.body.encoded).to match("You are yet to pay the rent for the room #{@room.room_name} for the period")
    end
  end
end
