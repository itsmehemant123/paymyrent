require 'spec_helper'

describe Api::V1::RoommatesController do

  describe "GET #show" do
    context "When room exists with single occupant" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room = FactoryGirl.create :room_with_person, person: @profile
        @profile.admin_room_id = @room.id
        api_authorization_header @user.auth_token
        get :show, id: @room.id
      end

      it "returns the information about the room" do
        room_response = json_response
        expect(room_response[:success]).to eql(true)
        expect(room_response[:data][:people].length).to eql(1)
        expect(room_response[:data][:people][0][:first_name]).to eql(@profile.first_name)
      end

      it {should respond_with 200}

    end

    context "When room exists with multiple occupants" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room = FactoryGirl.create :room_with_person, person: @profile
        @room.persons << FactoryGirl.create(:person)
        @profile.admin_room_id = @room.id
        api_authorization_header @user.auth_token
        get :show, id: @room.id
      end

      it "returns the information about the room" do
        room_response = json_response
        expect(room_response[:success]).to eql(true)
        expect(room_response[:data][:people].length).to eql(2)
      end

      it {should respond_with 200}

    end

    context "When room doesn't exist" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        api_authorization_header @user.auth_token
        get :show, id: 12
      end

      it "returns an error about the absence" do
        room_response = json_response
        expect(room_response[:errors]).to eql("Such a room doesn't exist")
      end

      it {should respond_with 422}
    end

    context "When user profile doesn't exist" do
      before(:each) do
        @user = FactoryGirl.create :user
        api_authorization_header @user.auth_token
        get :show, id: @user.id
      end

      it "returns an error about the profile" do
        room_response = json_response
        expect(room_response[:errors]).to eql("User Profile doesn't exist. Create a profile first")
      end

      it {should respond_with 422}

    end
  end

  describe "POST #add" do
    context "When email address added is already a user" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @user2 = FactoryGirl.create :user
        @profile2 = FactoryGirl.create :person, user: @user2
        @room = FactoryGirl.create :room
        @room.persons << @profile
        @profile.admin_room_id = @room.id
        @room.save
        @profile.save
        api_authorization_header @user.auth_token
        post :add, {id: @room.id, email: @user2.email}
      end

      it "should respond with success" do
        room_response = json_response
        room = Room.where(id: @room.id).first
        expect(room_response[:success]).to eql(true)
        expect(room.persons.count).to eql(2)
        #notification sent
        expect(Action.where(by: @user2.id).count()).to eql(1)
        notifHandle = ActionHandlers::NotificationActionHandle.new()
        notifHandle.load_action(by: @user2.id)
        notif = notifHandle.get_action!
        expect(Action.where(by: @user2.id).count()).to eql(0)
        expect(notif.count()).to eql(1)
        expect(notif[0][:type]).to eql('Info')
        expect(notif[0][:text]).to include("added to the room #{@room.room_name}")
        expect(notif[0][:text]).to include("by #{@profile.first_name}")
      end

      it {should respond_with 201}
    end

    context "When email address added is a new user" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room = FactoryGirl.create :room_with_person
        @room.persons << @profile
        @profile.admin_room_id = @room.id
        @room.save
        @profile.save
        api_authorization_header @user.auth_token
        post :add, {id: @room.id, email: "hemanth@paymyrent.in"}
      end

      it "should respond with success" do
        room_response = json_response
        expect(room_response[:success]).to eql(true)
        expect(Invite.where(email: "hemanth@paymyrent.in").first().room_id).to eql(@room.id)
        #notification sent
        invite_id = Invite.where(email: "hemanth@paymyrent.in").first().id
        expect(Action.where(from: invite_id).count()).to eql(1)
        notifHandle = ActionHandlers::NotificationActionHandle.new()
        notifHandle.load_action(by: @user.id)
        notif = notifHandle.get_action!
        expect(Action.where(by: @user.id).count()).to eql(0)
        expect(notif.count()).to eql(1)
        expect(notif[0][:type]).to eql('Info')
        expect(notif[0][:text]).to include('to hemanth@paymyrent.in to')
        expect(notif[0][:text]).to include(@room.room_name)
      end

      it {should respond_with 201}
    end

    #fix the english
    context "When email address added is already a user, and is already a room mate in another room" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @user2 = FactoryGirl.create :user
        @profile2 = FactoryGirl.create :person, user: @user2
        @room = FactoryGirl.create :room
        @room2 = FactoryGirl.create :room
        @room.persons << @profile
        @room2.persons << @profile2
        @profile.admin_room_id = @room.id
        @room.save
        @profile.save
        api_authorization_header @user.auth_token
        post :add, {id: @room.id, email: @user2.email}
      end

      it "should respond with error" do
        room_response = json_response
        expect(room_response[:success]).to eql(false)
        expect(room_response[:errors]).to eql("This user is in another room")
      end

      it {should respond_with 422}
    end

    context "When user is not a room mate" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room = FactoryGirl.create :room_with_person
        @profile.admin_room_id = @room.id
        @profile.save
        api_authorization_header @user.auth_token
        post :add, {id: @room.id, email: "hemanth"}
      end

      it "should respond with success" do
        room_response = json_response
        expect(room_response[:success]).to eql(false)
        expect(room_response[:errors]).to eql("User cannot add room-mates to this room")
      end

      it {should respond_with 422}
    end

    context "When user is not admin" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room = FactoryGirl.create :room_with_person
        @room.persons << @profile
        @room.save
        @profile.save
        api_authorization_header @user.auth_token
        post :add, {id: @room.id, email: "hemanth"}
      end

      it "should respond with error" do
        room_response = json_response
        expect(room_response[:success]).to eql(false)
        expect(room_response[:errors]).to eql("User cannot add room-mates to this room")
      end

      it {should respond_with 422}
    end

    context "When email address is missing" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room = FactoryGirl.create :room_with_person
        @room.persons << @profile
        @profile.admin_room_id = @room.id
        @room.save
        @profile.save
        api_authorization_header @user.auth_token
        post :add, {id: @room.id}
      end

      it "should respond with error" do
        room_response = json_response
        expect(room_response[:success]).to eql(false)
        expect(room_response[:errors]).to eql("Missing Email")
      end

      it {should respond_with 422}
    end

    context "When email address is invalid" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room = FactoryGirl.create :room_with_person
        @room.persons << @profile
        @profile.admin_room_id = @room.id
        @room.save
        @profile.save
        api_authorization_header @user.auth_token
        post :add, {id: @room.id, email: "hemanth"}
      end

      it "should respond with error" do
        room_response = json_response
        expect(room_response[:success]).to eql(false)
        expect(room_response[:errors]).to eql("Email invalid")
      end

      it {should respond_with 422}
    end
  end

  describe "POST #remove" do
    context "When the User is admin, and sent the correct email" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @user2 = FactoryGirl.create :user
        @profile2 = FactoryGirl.create :person, user: @user2
        @room = FactoryGirl.create :room
        @room.persons << @profile
        @room.persons << @profile2
        @profile.admin_room_id = @room.id
        @room.save
        @profile.save
        api_authorization_header @user.auth_token
        post :remove, {id: @room.id, email: @user2.email}
      end

      it "should respond with success" do
        room_response = json_response
        room = Room.where(id: @room.id).first
        expect(room_response[:success]).to eql(true)
        expect(room.persons.where(id: @profile2.id).count()).to eql(0)
        expect(room.persons.count()).to eql(1)
        #Notification Pushed
        expect(Action.where(by: @user2.id).count()).to eql(1)
        notifHandle = ActionHandlers::NotificationActionHandle.new()
        notifHandle.load_action(by: @user2.id)
        notif = notifHandle.get_action!
        expect(Action.where(by: @user2.id).count()).to eql(0)
        expect(notif.count()).to eql(1)
        expect(notif[0][:type]).to eql('Info')
        expect(notif[0][:text]).to include(@room.room_name)
        expect(notif[0][:text]).to include('removed')
        expect(notif[0][:text]).to include("by #{@profile.first_name}")
      end

      it {should respond_with 200}
    end

    context "When the User is admin, and the user isn't in the room" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @user2 = FactoryGirl.create :user
        @profile2 = FactoryGirl.create :person, user: @user2
        @room = FactoryGirl.create :room
        @room2 = FactoryGirl.create :room
        @room.persons << @profile
        @room2.persons << @profile2
        @profile.admin_room_id = @room.id
        @room.save
        @room2.save
        @profile.save
        api_authorization_header @user.auth_token
        post :remove, {id: @room.id, email: @user2.email}
      end

      it "should respond with success with no change to the rooms" do
        room_response = json_response
        room = Room.where(id: @room.id).first
        expect(room_response[:success]).to eql(true)
        expect(room.persons.count()).to eql(1)
      end

      it {should respond_with 200}
    end

    context "When the User is admin, and sent the wrong email" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @user2 = FactoryGirl.create :user
        @profile2 = FactoryGirl.create :person, user: @user2
        @room = FactoryGirl.create :room
        @room.persons << @profile
        @room.persons << @profile2
        @profile.admin_room_id = @room.id
        @room.save
        @profile.save
        api_authorization_header @user.auth_token
        post :remove, {id: @room.id, email: 'lolcats@dankmemes.com'}
      end

      it "should respond with success but no change to room" do
        room_response = json_response
        room = Room.where(id: @room.id).first
        expect(room_response[:success]).to eql(true)
        expect(room.persons.count()).to eql(2)
      end

      it {should respond_with 200}
    end


    context "When email address is missing" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room = FactoryGirl.create :room_with_person
        @room.persons << @profile
        @profile.admin_room_id = @room.id
        @room.save
        @profile.save
        api_authorization_header @user.auth_token
        post :remove, {id: @room.id}
      end

      it "should respond with error" do
        room_response = json_response
        expect(room_response[:success]).to eql(false)
        expect(room_response[:errors]).to eql("Missing Email")
      end

      it {should respond_with 422}
    end

    context "When email address is invalid" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room = FactoryGirl.create :room_with_person
        @room.persons << @profile
        @profile.admin_room_id = @room.id
        @room.save
        @profile.save
        api_authorization_header @user.auth_token
        post :remove, {id: @room.id, email: "hemanth"}
      end

      it "should respond with error" do
        room_response = json_response
        expect(room_response[:success]).to eql(false)
        expect(room_response[:errors]).to eql("Email invalid")
      end

      it {should respond_with 422}
    end

    context "When user is not a room mate" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room = FactoryGirl.create :room_with_person
        @profile.admin_room_id = @room.id
        @profile.save
        api_authorization_header @user.auth_token
        post :remove, {id: @room.id, email: "hemanth"}
      end

      it "should respond with success" do
        room_response = json_response
        expect(room_response[:success]).to eql(false)
        expect(room_response[:errors]).to eql("User cannot add room-mates to this room")
      end

      it {should respond_with 422}
    end

    context "When user is not admin" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room = FactoryGirl.create :room_with_person
        @room.persons << @profile
        @room.save
        @profile.save
        api_authorization_header @user.auth_token
        post :remove, {id: @room.id, email: "hemanth"}
      end

      it "should respond with error" do
        room_response = json_response
        expect(room_response[:success]).to eql(false)
        expect(room_response[:errors]).to eql("User cannot add room-mates to this room")
      end

      it {should respond_with 422}
    end
  end

  describe "POST #join" do
    context "When user submits a room join request" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @user2 = FactoryGirl.create :user
        @profile2 = FactoryGirl.create :person, user: @user2
        @user3 = FactoryGirl.create :user
        @profile3 = FactoryGirl.create :person, user: @user3
        @room = FactoryGirl.create :room
        @room.persons << @profile
        @room.persons << @profile2
        @profile.admin_room_id = @room.id
        @room.save
        @profile.save
        api_authorization_header @user3.auth_token
        post :join, {id: @room.id}
      end

      it "should create the notification for the admin" do
        notifHandle = ActionHandlers::NotificationActionHandle.new()
        expect(Action.where(by: @profile.user_id).count()).to eql(1)
        expect(Action.where(action: ActionHandlers::ActionTypes::SHOULD_USERJOINROOMREQUEST).count()).to eql(1)
        notifHandle.load_action(by: @profile.user_id)
        resp = notifHandle.get_action!
        expect(Action.where(by: @profile.user_id).count()).to eql(1)
        act_obj = Action.where(action: ActionHandlers::ActionTypes::SHOULD_USERJOINROOMREQUEST)
        expect(act_obj.count()).to eql(1)
        expect(resp.count()).to eql(1)
        expect(resp[0][:type]).to eql('Response')
        expect(resp[0][:text]).to include("The user #{@profile3.first_name}")
        expect(resp[0][:text]).to include("join #{@room.room_name}")
        expect(resp[0]).to have_key(:response_options)
        expect(resp[0][:response_options]).to have_key(:YES)
        expect(resp[0][:response_options]).to have_key(:NO)
        expect(resp[0][:response_options][:YES]).to include("/actions/#{act_obj.first().id}/YES")
        expect(resp[0][:response_options][:NO]).to include("/actions/#{act_obj.first().id}/NO")
      end

      it {should respond_with 201}
    end

    context "when user submits a room join request with no admin" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @user2 = FactoryGirl.create :user
        @profile2 = FactoryGirl.create :person, user: @user2
        @user3 = FactoryGirl.create :user
        @profile3 = FactoryGirl.create :person, user: @user3
        @room = FactoryGirl.create :room
        @room.persons << @profile
        @room.persons << @profile2
        @room.save
        @profile.save
        api_authorization_header @user3.auth_token
        post :join, {id: @room.id}
      end

      it "should send the error" do
        resp = json_response
        expect(resp[:success]).to eql(false)
        expect(resp[:errors]).to eql("This room doesn't have an admin")
      end

      it {should respond_with 422}
    end

    context "When room doesn't exist" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        api_authorization_header @user.auth_token
        get :join, id: 12
      end

      it "returns an error about the absence" do
        room_response = json_response
        expect(room_response[:errors]).to eql("Such a room doesn't exist")
      end

      it {should respond_with 422}
    end

    context "When user profile doesn't exist" do
      before(:each) do
        @user = FactoryGirl.create :user
        api_authorization_header @user.auth_token
        get :join, id: @user.id
      end

      it "returns an error about the profile" do
        room_response = json_response
        expect(room_response[:errors]).to eql("User Profile doesn't exist. Create a profile first")
      end

      it {should respond_with 422}

    end
  end

  describe "POST #leave" do
    context "when user leaves the room" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @user2 = FactoryGirl.create :user
        @profile2 = FactoryGirl.create :person, user: @user2
        @user3 = FactoryGirl.create :user
        @profile3 = FactoryGirl.create :person, user: @user3
        @room = FactoryGirl.create :room
        @room.persons << @profile
        @room.persons << @profile2
        @room.persons << @profile3
        @profile.admin_room_id = @room.id
        @room.save
        @profile.save
        api_authorization_header @user3.auth_token
        post :leave, {id: @room.id}
      end

      it "should create notifications for all members" do
        notifHandler = ActionHandlers::NotificationActionHandle.new()
        expect(Action.where(action: ActionHandlers::ActionTypes::INFO_USERLEFTROOM).count()).to eql(2)
        notifHandler.load_action(by: @user2.id)
        notif = notifHandler.get_action!
        expect(Action.where(action: ActionHandlers::ActionTypes::INFO_USERLEFTROOM).count()).to eql(1)
        expect(notif.count()).to eql(1)
        expect(notif[0][:type]).to eql('Info')
        expect(notif[0][:text]).to eql("User #{@profile3.first_name} has left the room #{@room.room_name}")
      end

      it {should respond_with 201}
    end

    context "when admin leaves the room" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @user2 = FactoryGirl.create :user
        @profile2 = FactoryGirl.create :person, user: @user2
        @user3 = FactoryGirl.create :user
        @profile3 = FactoryGirl.create :person, user: @user3
        @room = FactoryGirl.create :room
        @room.persons << @profile
        @room.persons << @profile2
        @room.persons << @profile3
        @profile.admin_room_id = @room.id
        @room.save
        @profile.save
        api_authorization_header @user.auth_token
        post :leave, {id: @room.id}
      end

      it "should create notifications for all members, make another user admin along with the notification" do
        notifHandler = ActionHandlers::NotificationActionHandle.new()
        expect(Action.where(action: ActionHandlers::ActionTypes::INFO_USERLEFTROOM).count()).to eql(2)
        expect(Action.where(action: ActionHandlers::ActionTypes::INFO_USERISROOMADMIN).count()).to eql(1)
        notifHandler.load_action(by: @user2.id)
        notif = notifHandler.get_action!
        expect(Action.where(action: ActionHandlers::ActionTypes::INFO_USERLEFTROOM).count()).to eql(1)
        expect(Action.where(action: ActionHandlers::ActionTypes::INFO_USERISROOMADMIN).count()).to eql(0)
        expect(notif.count()).to eql(2)
      end

      it {should respond_with 201}
    end

    context "when admin leaves an empty room" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room = FactoryGirl.create :room
        @room.persons << @profile
        @profile.admin_room_id = @room.id
        @room.save
        @profile.save
        api_authorization_header @user.auth_token
        post :leave, {id: @room.id}
      end

      it "should show errors" do
        room_response = json_response
        expect(room_response[:success]).to eql(false)
        expect(room_response[:errors]).to eql('You are the only member of the room. Delete the room instead.')
      end

      it {should respond_with 422}
    end

    context "when user leaves the room, but isn't a part of the room" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @user2 = FactoryGirl.create :user
        @profile2 = FactoryGirl.create :person, user: @user2
        @user3 = FactoryGirl.create :user
        @profile3 = FactoryGirl.create :person, user: @user3
        @room = FactoryGirl.create :room
        @room.persons << @profile
        @room.persons << @profile2
        @profile.admin_room_id = @room.id
        @room.save
        @profile.save
        api_authorization_header @user3.auth_token
        post :leave, {id: @room.id}
      end

      it "should give error" do
        resp = json_response
        expect(resp[:success]).to eql(false)
        expect(resp[:errors]).to eql('This user is not in the room')
      end

      it {should respond_with 422}
    end

    context "When room doesn't exist" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        api_authorization_header @user.auth_token
        get :leave, id: 12
      end

      it "returns an error about the absence" do
        room_response = json_response
        expect(room_response[:errors]).to eql("Such a room doesn't exist")
      end

      it {should respond_with 422}
    end

    context "When user profile doesn't exist" do
      before(:each) do
        @user = FactoryGirl.create :user
        api_authorization_header @user.auth_token
        get :leave, id: @user.id
      end

      it "returns an error about the profile" do
        room_response = json_response
        expect(room_response[:errors]).to eql("User Profile doesn't exist. Create a profile first")
      end

      it {should respond_with 422}

    end
  end
end
