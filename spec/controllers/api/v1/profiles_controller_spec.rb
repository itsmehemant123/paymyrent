require 'spec_helper'

describe Api::V1::ProfilesController do

  describe "GET #show" do
    context "When profile is created" do
      before(:each) do
        @user = FactoryGirl.create :user
        @person = FactoryGirl.create :person, user: @user
        api_authorization_header @user.auth_token
        get :show
      end

      it "returns the information about the person profile" do
        person_response = json_response
        expect(person_response[:data][:profile][:first_name]).to eql(@user.person.first_name)
      end

      it {should respond_with 200}
    end

    context "When profile is not created" do
      before(:each) do
        @user = FactoryGirl.create :user
        api_authorization_header @user.auth_token
        get :show
      end

      it "returns error with the information about the error" do
        error_response = json_response
        expect(error_response[:errors]).to eql("Profile not yet created")
      end

      it {should respond_with 422}
    end
  end

  describe "POST #create" do
    context "when succesfully created" do
      before(:each) do
        user = FactoryGirl.create :user
        @profile_attributes = FactoryGirl.attributes_for :person
        api_authorization_header user.auth_token
        post :create, {profile: @profile_attributes}
      end

      it "renders the json of the profile just created" do
        profile_response = json_response
        expect(profile_response[:data][:profile][:first_name]).to eql(@profile_attributes[:first_name])
      end

      it {should respond_with 201}
    end

    context "when succesfully created via an invite" do
      before(:each) do
        @user = FactoryGirl.create :user
        @room = FactoryGirl.create :room
        @profile_attributes = FactoryGirl.attributes_for :person
        @user1 = FactoryGirl.create :user
        @profile1 = FactoryGirl.create :person, user: @user1
        @room.persons << @profile1
        @room.save
        @profile1.admin_room_id = @room.id
        @profile1.save
        @invite = FactoryGirl.create :invite
        @invite.email = @user.email
        @invite.room_id = @room.id
        @invite.save
        ActionHandlers::NotificationActionHandle.new().addNotification(from: @invite.id, by: @profile1.user_id, target: @room.id, type: ActionHandlers::ActionTypes::INFO_ROOMJOININVITESENT)
        api_authorization_header @user.auth_token
        post :create, {profile: @profile_attributes}
      end

      it "renders the json of the profile just created with invites cleared and user added to the room" do
        profile_response = json_response
        room = Room.where(id: @room.id).first()
        invite = Invite.where(email: @user.email)
        expect(profile_response[:data][:profile][:first_name]).to eql(@profile_attributes[:first_name])
        expect(room.persons.count()).to eql(2)
        expect(room.persons.where(id: profile_response[:data][:profile][:id]).first().first_name).to eql(@profile_attributes[:first_name])
        expect(room.persons.where(id: profile_response[:data][:profile][:id]).count()).to eql(1)
        expect(invite.count()).to eql(0)
      end

      it {should respond_with 201}
    end

    context "when unable to create" do
      before(:each) do
        user = FactoryGirl.create :user
        @profile_attributes = FactoryGirl.attributes_for :person
        api_authorization_header user.auth_token

        #except - used to be address, changed to first_name for the facebook integration
        post :create, {profile: @profile_attributes.except(:first_name)}
      end

      it "renders the errors json" do
        profile_response = json_response
        expect(profile_response ).to have_key(:errors)
      end

      it "renders the errors occured during the creation" do
        profile_response = json_response

        #except - used to be address, changed to first_name for the facebook integration
        expect(profile_response[:errors][:first_name]).to eql(["can't be blank"])
      end

      it {should respond_with 422}
    end
  end

  describe "PUT/PATCH #update" do

    context "when is succesfully updated" do
      before(:each) do
        user = FactoryGirl.create :user
        profile = FactoryGirl.create :person, user: user
        api_authorization_header user.auth_token
        patch :update, {profile: {first_name: "hemanth"}}
      end

      it "renders the correct, updated json response for the profile" do
        profile_response = json_response
        expect(profile_response[:data][:profile][:first_name]).to eql "hemanth"
      end

      it {should respond_with 200}
    end

    context "when is not updated" do
      before(:each) do
        user = FactoryGirl.create :user
        profile = FactoryGirl.create :person, user: user
        api_authorization_header user.auth_token
        patch :update, {profile: {first_name: ""}}
      end

      it "renders an errors json" do
        profile_response = json_response
        expect(profile_response).to have_key(:errors)
      end

      it "renders the json errors on why the user could not be created" do
        profile_response = json_response
        expect(profile_response[:errors][:first_name]).to include "can't be blank"
      end

      it { should respond_with 422 }
    end
  end

  describe "GET #search" do
    context "When a user submits a search query on name" do
      before(:each) do
        @user1 = FactoryGirl.create :user
        @profile1 = FactoryGirl.create :person, user: @user1
        @profile1.first_name = 'fname1'
        @profile1.last_name = 'lname1'
        @profile1.save
        @user2 = FactoryGirl.create :user
        @profile2 = FactoryGirl.create :person, user: @user2
        @profile2.first_name = 'abc'
        @profile2.last_name = 'xyz'
        @profile2.save
        @user3 = FactoryGirl.create :user
        @profile3 = FactoryGirl.create :person, user: @user3
        @profile3.first_name = 'fname3'
        @profile3.last_name = 'lname3'
        @profile3.save
        @user4 = FactoryGirl.create :user
        @profile4 = FactoryGirl.create :person, user: @user4
        @profile4.first_name = 'pqr'
        @profile4.last_name = 'hcf'
        @profile4.save
        api_authorization_header @user1.auth_token
        get :search, {query: 'nam'}
      end

      it "should respond with the results" do
        search_response = json_response
        expect(search_response[:success]).to eql(true)
        expect(search_response[:data][:people].count()).to eql(2)
      end

      it {should respond_with 200}
    end

    context "When the user searches for email" do
      before(:each) do
        @user1 = FactoryGirl.create :user
        @profile1 = FactoryGirl.create :person, user: @user1
        @profile1.first_name = 'fname1'
        @profile1.last_name = 'lname1'
        @profile1.save
        @user1.email = 'user1@pmr.in'
        @user1.save
        @user2 = FactoryGirl.create :user
        @profile2 = FactoryGirl.create :person, user: @user2
        @profile2.first_name = 'abc'
        @profile2.last_name = 'xyz'
        @profile2.save
        @user2.email = 'factory@rm.in'
        @user2.save
        @user3 = FactoryGirl.create :user
        @profile3 = FactoryGirl.create :person, user: @user3
        @profile3.first_name = 'fname3'
        @profile3.last_name = 'lname3'
        @profile3.save
        @user3.email = 'user3@in.com'
        @user3.save


        api_authorization_header @user1.auth_token
        get :search, {query: '@pmr'}
      end

      it "should respond with the results" do
        search_response = json_response
        expect(search_response[:success]).to eql(true)
        expect(search_response[:data][:people].count()).to eql(1)
      end

      it {should respond_with 200}
    end

  end

end
