require 'spec_helper'

RSpec.describe Api::V1::ActionsController, type: :controller do

  describe "POST #do_action" do

    context "a new action is created manually" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room = FactoryGirl.create :room_with_person, person: @profile
        @profile.admin_room_id = @room.id
        @action = FactoryGirl.create :action
        @action.from = 1
        @action.by = @user.id
        @action.target = @room.id
        @action.action = ActionHandlers::ActionTypes::INFO_INVITEUSERJOINED
        @action.save
        api_authorization_header @user.auth_token
        #post :do_action, id: @action.id
      end

      it "should create the correct object" do
        action_obj = Action.where(id: @action.id).first
        expect(action_obj.by).to eql(@user.id)
        expect(action_obj.action).to eql('1')
      end
    end

    context "a new action is created" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room = FactoryGirl.create :room_with_person, person: @profile
        @profile.admin_room_id = @room.id
        @action = ActionHandlers::NotificationActionHandle.new().addNotification(from: 1, by: @user.id, target: @room.id, type: ActionHandlers::ActionTypes::INFO_INVITEUSERJOINED)
        api_authorization_header @user.auth_token
        #post :do_action, id: @action.id
      end

      it "should create the correct object" do
        action_obj = Action.where(id: @action.id).first
        expect(action_obj.by).to eql(@user.id)
        expect(action_obj.action).to eql('1')
        expect(action_obj.target).to eql(@room.id)

        action_obj = Action.where(action: ActionHandlers::ActionTypes::INFO_INVITEUSERJOINED).first

        expect(action_obj.by).to eql(@user.id)
        expect(action_obj.action).to eql('1')
        expect(action_obj.target).to eql(@room.id)

      end
    end

    context "a created action is destroyed" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room = FactoryGirl.create :room_with_person, person: @profile
        @profile.admin_room_id = @room.id
        obj = ActionHandlers::NotificationActionHandle.new()
        @action = obj.addNotification(from: 1, by: @user.id, target: @room.id, type: ActionHandlers::ActionTypes::INFO_INVITEUSERJOINED)
        api_authorization_header @user.auth_token
        obj = ActionHandlers::NotificationActionHandle.new()
        obj.load_action_by_id(@action.id)
        obj.clear_action
        #post :do_action, id: @action.id
      end

      it "should destroy the object" do
        action_obj = Action.where(id: @action.id).first
        expect(action_obj.nil?).to eql(true)
        expect(Action.all().count()).to eql(0)
      end
    end

    context "get and destroy action is tested on info and user actions" do
      before(:each) do
        obj = ActionHandlers::NotificationActionHandle.new()
        @user1 = FactoryGirl.create :user
        @person1 = FactoryGirl.create :person, user: @user1
        @user2 = FactoryGirl.create :user
        @person2 = FactoryGirl.create :person, user: @user2
        @room = FactoryGirl.create :room
        @action = obj.addNotification(from: @user1.id, by: @user2.id, target: @room.id, type: ActionHandlers::ActionTypes::INFO_INVITEUSERJOINED)
        @action2 = obj.addNotification(from: @user1.id, by: @user2.id, target: @room.id, type: ActionHandlers::ActionTypes::INFO_ROOMJOINREQUESTACCEPTED)
        @action3 = obj.addNotification(from: @user1.id, by: @user2.id, target: @room.id, type: ActionHandlers::ActionTypes::SHOULD_USERJOINROOMREQUEST)
        @before_count = Action.all().count()
        obj.load_action(by: @user2.id)
        @res = obj.get_action!
        @count = @res.count()
        @after_count = Action.all().count()
        #post :do_action, id: @action.id
      end

      it "should remove the INFO actions and leave USER actions" do
        action_list = Action.where(by: @user2.id)
        expect(Action.all().count()).to eql(1)
        expect(action_list.count()).to eql(1)
        expect(@before_count - @after_count).to eql(2)
        expect(@after_count).to eql(1)
        expect(@count).to eql(3)
        expect(@res[0]).to have_key(:text)
        expect(@res[0]).to have_key(:type)
        expect(@res[1]).to have_key(:text)
        expect(@res[1]).to have_key(:type)
        expect(@res[2]).to have_key(:text)
        expect(@res[2]).to have_key(:type)
      end
    end

    context "get all actions is tested" do
      before(:each) do
        obj = ActionHandlers::NotificationActionHandle.new()
        @user1 = FactoryGirl.create :user
        @person1 = FactoryGirl.create :person, user: @user1
        @user2 = FactoryGirl.create :user
        @person2 = FactoryGirl.create :person, user: @user2
        @room = FactoryGirl.create :room
        @action = obj.addNotification(from: @user1.id, by: @user2.id, target: @room.id, type: ActionHandlers::ActionTypes::INFO_INVITEUSERJOINED)
        @action2 = obj.addNotification(from: @user1.id, by: @user2.id, target: @room.id, type: ActionHandlers::ActionTypes::INFO_ROOMJOINREQUESTACCEPTED)
        @action3 = obj.addNotification(from: @user1.id, by: @user2.id, target: @room.id, type: ActionHandlers::ActionTypes::SHOULD_USERJOINROOMREQUEST)
        @before_count = Action.all().count()
        obj.load_action(by: @user2.id)
        @res = obj.get_action
        @count = @res.count()
        @after_count = Action.all().count()
        #post :do_action, id: @action.id
      end

      it "should leave both the INFO and USER actions" do
        action_list = Action.where(by: @user2.id)
        expect(Action.all().count()).to eql(3)
        expect(action_list.count()).to eql(3)
        expect(@before_count - @after_count).to eql(0)
        expect(@after_count).to eql(3)
        expect(@count).to eql(3)
      end
    end

    context "user room join scenario is initiated, accept condition tested" do
      before(:each) do
        obj = ActionHandlers::NotificationActionHandle.new()
        @user1 = FactoryGirl.create :user
        @person1 = FactoryGirl.create :person, user: @user1
        @user2 = FactoryGirl.create :user
        @person2 = FactoryGirl.create :person, user: @user2
        @room = FactoryGirl.create :room
        @user3 = FactoryGirl.create :user
        @person3 = FactoryGirl.create :person, user: @user3

        @room.persons << @user2.person
        @room.persons << @user3.person
        @room.save

        @action = obj.addNotification(from: @user1.id, by: @user2.id, target: @room.id, type: ActionHandlers::ActionTypes::SHOULD_USERJOINROOMREQUEST)

        api_authorization_header @user2.auth_token
        post :do_action, {id: @action.id, verb: 'YES'}
      end

      it "it should remove the user action, generate a new info action about the acceptance" do
        resp = json_response
        expect(resp[:success]).to eql(true)
        expect(Action.where(action: ActionHandlers::ActionTypes::SHOULD_USERJOINROOMREQUEST).count()).to eql(0)
        expect(Action.where(action: ActionHandlers::ActionTypes::INFO_ROOMJOINREQUESTACCEPTED).count()).to eql(1)
        expect(Action.where(action: ActionHandlers::ActionTypes::INFO_ROOMJOINREQUESTREJECTED).count()).to eql(0)
        expect(Action.where(by: @user1.id).count()).to eql(1)
        expect(Action.where(by: @user2.id).count()).to eql(1)
        expect(Room.where(id: @room.id).first().persons.count()).to eql(3)
        expect(Action.where(by: @user3.id).count()).to eql(1)
        expect(Action.where(action: ActionHandlers::ActionTypes::INFO_USERJOINEDROOM).count()).to eql(2)
      end
    end

    context "user room join scenario is initiated, reject condition tested" do
      before(:each) do
        obj = ActionHandlers::NotificationActionHandle.new()
        @user1 = FactoryGirl.create :user
        @person1 = FactoryGirl.create :person, user: @user1
        @user2 = FactoryGirl.create :user
        @person2 = FactoryGirl.create :person, user: @user2
        @room = FactoryGirl.create :room

        @action = obj.addNotification(from: @user1.id, by: @user2.id, target: @room.id, type: ActionHandlers::ActionTypes::SHOULD_USERJOINROOMREQUEST)

        api_authorization_header @user2.auth_token
        post :do_action, {id: @action.id, verb: 'NO'}
      end

      it "it should remove the user action, generate a new info action about the rejection" do
        resp = json_response
        expect(resp[:success]).to eql(true)
        expect(Action.where(action: ActionHandlers::ActionTypes::SHOULD_USERJOINROOMREQUEST).count()).to eql(0)
        expect(Action.where(action: ActionHandlers::ActionTypes::INFO_ROOMJOINREQUESTACCEPTED).count()).to eql(0)
        expect(Action.where(action: ActionHandlers::ActionTypes::INFO_ROOMJOINREQUESTREJECTED).count()).to eql(1)
        expect(Action.where(by: @user1.id).count()).to eql(1)
        expect(Action.where(by: @user2.id).count()).to eql(0)
        expect(Action.where(action: ActionHandlers::ActionTypes::INFO_USERJOINEDROOM).count()).to eql(0)
      end
    end

    context "Wrong action id provided" do
      before(:each) do
        obj = ActionHandlers::NotificationActionHandle.new()
        @user1 = FactoryGirl.create :user
        @person1 = FactoryGirl.create :person, user: @user1
        @user2 = FactoryGirl.create :user
        @person2 = FactoryGirl.create :person, user: @user2
        @room = FactoryGirl.create :room

        @action = obj.addNotification(from: @user1.id, by: @user2.id, target: @room.id, type: ActionHandlers::ActionTypes::SHOULD_USERJOINROOMREQUEST)

        api_authorization_header @user2.auth_token
        post :do_action, {id: -1, verb: 'YES'}
      end

      it "should provide the details of the error" do
        resp = json_response
        expect(resp[:success]).to eql(false)
        expect(resp[:errors]).to eql('No such action exists')
      end
    end

  end

end
