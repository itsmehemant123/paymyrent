require 'spec_helper'

RSpec.describe Api::V1::NotificationsController, type: :controller do
  describe "GET #show" do
    context "when user does a get" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @user2 = FactoryGirl.create :user
        @profile2 = FactoryGirl.create :person, user: @user2
        @room = FactoryGirl.create :room_with_person, person: @profile
        @profile.admin_room_id = @room.id

        ActionHandlers::NotificationActionHandle.new().addNotification(from: @user2.id, by: @user.id, target: @room.id, type: ActionHandlers::ActionTypes::SHOULD_USERJOINROOMREQUEST)
        ActionHandlers::NotificationActionHandle.new().addNotification(from: @user2.id, by: @user.id, target: @room.id, type: ActionHandlers::ActionTypes::INFO_INVITEUSERJOINED)


        api_authorization_header @user.auth_token
        get :show
      end

      it "should create the required notifications, and destroy INFO notifications on retrieval" do
        notification_response = json_response
        expect(notification_response[:success]).to eql(true)
        expect(notification_response[:data][:notifications].count()).to eql(2)
        expect(Action.where(by: @user.id).count()).to eql(1)
      end

      it {should respond_with 200}
    end
  end
end
