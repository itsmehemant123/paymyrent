require 'spec_helper'

describe Api::V1::RoomsController do

  describe "GET #show" do
    context "When room exists" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room = FactoryGirl.create :room_with_person, person: @profile
        api_authorization_header @user.auth_token
        get :show, id: @user.id
      end

      it "returns the information about the room" do
        room_response = json_response
        expect(room_response[:data][:room][:room_name]).to eql(@user.person.rooms.first.room_name)
      end

      it {should respond_with 200}

    end

    context "When room doesn't exist" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        api_authorization_header @user.auth_token
        get :show, id: @user.id
      end

      it "returns an error about the absence" do
        room_response = json_response
        expect(room_response[:errors]).to eql("User's Profile doesn't have any rooms")
      end

      it {should respond_with 422}
    end

    context "When user profile doesn't exist" do
      before(:each) do
        @user = FactoryGirl.create :user
        api_authorization_header @user.auth_token
        get :show, id: @user.id
      end

      it "returns an error about the profile" do
        room_response = json_response
        expect(room_response[:errors]).to eql("User Profile doesn't exist. Create a profile first")
      end

      it {should respond_with 422}

    end

  end

  describe "POST #create" do
    context "when profile exists, and all the required details are available, and profile has no rooms" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room_attributes = FactoryGirl.attributes_for :room
        api_authorization_header @user.auth_token
        post :create, {room: @room_attributes}
      end

      it "returns the created room json with correct associations" do
        room_response = json_response
        #puts("ROOM ATTRBS:#{@room_attributes}")
        #puts("RESP:#{room_response}")
        #puts("USER:#{@user.to_json}")
        #puts("PROFILE:#{@profile.to_json}")
        #puts("\nVerifying rooms count in user.person:#{@user.person.rooms.count} and user.person.room.id: #{@user.person.rooms.first.id} and room id: #{room_response[:room][:id]}")
        #puts("\nVerifying rooms count in person:#{@profile.rooms.count} and user.person.room.id: #{@profile.rooms.first.id} and room id: #{room_response[:room][:id]}")
        #puts("\nCheck contents of person in room #{@profile.rooms.first.persons.first.id} with person #{@profile.id}")
        #check room_attributes and the returned json
        #puts("CLASS CHECK: PROFILE ROOM DATE CLASS - #{@profile.rooms.first.pay_period_start.class} ATTRIB CLASS - #{@room_attributes[:pay_period_start].class}")
        expect(@user.person.rooms.first.id).to eql(room_response[:data][:room][:id])
        expect(@profile.rooms.first.id).to eql(room_response[:data][:room][:id])
        expect(@room_attributes[:rent]).to eql(room_response[:data][:room][:rent].to_f)
        expect(@room_attributes[:room_name]).to eql(room_response[:data][:room][:room_name])
        expect(@room_attributes[:owner_first_name]).to eql(room_response[:data][:room][:owner_first_name])
        expect(DateTimeConvertor.convertRESTDateTimeToDate(@room_attributes[:pay_period_start])).to eql(DateTimeConvertor.convertDbDateTimeToDate(room_response[:data][:room][:pay_period_start]))
        expect(@room_attributes[:pay_period_type]).to eql(room_response[:data][:room][:pay_period_type])
        #check room_attributes and the profile/user room
        profile = Person.where(id: @profile.id).first()
        expect(@room_attributes[:rent]).to eql(profile.rooms.first.rent.to_f)
        expect(@room_attributes[:room_name]).to eql(profile.rooms.first.room_name)
        expect(@room_attributes[:owner_first_name]).to eql(profile.rooms.first.owner_first_name)
        expect(DateTimeConvertor.convertRESTDateTimeToDate(@room_attributes[:pay_period_start])).to eql(DateTimeConvertor.convertDbDateTimeToDate(profile.rooms.first.pay_period_start))
        expect(@room_attributes[:pay_period_type]).to eql(profile.rooms.first.pay_period_type)
        expect(room_response[:success]).to eql(true)

      end

      it "ensures that the user is the admin of the created room" do
        room_response = json_response
        #puts("RESP:#{room_response}")
        user = User.where(id: @user.id).first()
        profile = Person.where(id: @profile.id).first()
        expect(user.person.admin_room_id).to eql(room_response[:data][:room][:id])
        expect(profile.admin_room_id).to eql(room_response[:data][:room][:id])

      end

      it {should respond_with 201}
    end

    context "when profile exists, and all the required details are available, and profile is room mate" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room_attributes = FactoryGirl.attributes_for :room
        room = FactoryGirl.create :room
        @profile.rooms << room
        @profile.save
        #puts("COUNT: #{@profile.rooms.count()} and #{room.persons.count()} with #{@user.person.rooms.count()}")
        api_authorization_header @user.auth_token
        post :create, {room: @room_attributes}
      end

      it "should return error saying the profile already has a room" do

        room_response = json_response
        expect(room_response[:success]).to eql(false)
        expect(room_response[:errors]).to eql("Profile already has a room")
      end

      it {should respond_with 422}
    end

    context "when profile exists, and the profile is an admin of a room" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room_attributes = FactoryGirl.attributes_for :room
        @profile.admin_room_id = 10
        @profile.save
        #puts("COUNT: #{@profile.rooms.count()} and #{room.persons.count()} with #{@user.person.rooms.count()}")
        api_authorization_header @user.auth_token
        post :create, {room: @room_attributes}
      end

      it "should return error saying the profile already has a room" do

        room_response = json_response
        expect(room_response[:success]).to eql(false)
        expect(room_response[:errors]).to eql("Profile already has a room")
      end

      it {should respond_with 422}
    end

    context "when profile doesn't exist" do
      before(:each) do
        @user = FactoryGirl.create :user
        @room_attributes = FactoryGirl.attributes_for :room
        api_authorization_header @user.auth_token
        post :create, {room: @room_attributes}
      end

      it "should return error saying the profile doesn't exist" do

        room_response = json_response
        expect(room_response[:success]).to eql(false)
        expect(room_response[:errors]).to eql("User Profile doesn't exist. Create a profile first")
      end

      it {should respond_with 422}
    end

    context "when profile exists, but all parameters aren't sent" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room_attributes = FactoryGirl.attributes_for :room
        @room_attributes[:owner_bank_account] = nil
        api_authorization_header @user.auth_token
        post :create, {room: @room_attributes}
      end

      it "should return error" do

        room_response = json_response
        expect(room_response[:success]).to eql(false)
        expect(room_response[:errors]).to have_key(:owner_bank_account)
        expect(room_response[:errors][:owner_bank_account]).to include "can't be blank"
      end

      it {should respond_with 422}
    end

    context "when profile exists, and user tries to create an existing room" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room_attributes = FactoryGirl.attributes_for :room
        Room.create(@room_attributes)
        #puts("COUNT: #{@profile.rooms.count()} and #{room.persons.count()} with #{@user.person.rooms.count()}")
        api_authorization_header @user.auth_token
        post :create, {room: @room_attributes}
      end

      it "should return error saying that the room already exists" do

        room_response = json_response
        expect(room_response[:success]).to eql(false)
        expect(room_response[:errors]).to eql("This room already exists")
      end

      it {should respond_with 422}
    end
  end

  describe "PUT/PATCH #update " do
    context "When profile and room exists, and all the required details are sent" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room = FactoryGirl.create :room
        @profile.rooms << @room
        @profile.admin_room_id = @room.id
        @profile.save

        api_authorization_header @user.auth_token
        patch :update, {id: @room.id, room: {owner_first_name: "hemanth"} }
      end

      it "should respond with the updated room" do
        room_response = json_response
        room = Room.where(id: @room.id).first
        expect(room_response[:success]).to eql(true)
        expect(room.owner_first_name).to eql("hemanth")
      end

      it {should respond_with 200}
    end

    context "When profile and room exists, and all the required details are sent, but repeated" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room = FactoryGirl.create :room
        @room2 = FactoryGirl.create :room
        @room2.owner_bank_account = 'hemanth'
        @room2.save
        @profile.rooms << @room
        @profile.admin_room_id = @room.id
        @profile.save

        api_authorization_header @user.auth_token
        patch :update, {id: @room.id, room: {owner_bank_account: "hemanth"} }
      end

      it "should respond with the error" do
        room_response = json_response
        expect(room_response[:success]).to eql(false)
        expect(room_response[:errors]).to eql('This room already exists')
      end

      it {should respond_with 422}
    end

    context "When profile and room exists, but user is not admin" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room = FactoryGirl.create :room
        @profile.rooms << @room
        @profile.save

        api_authorization_header @user.auth_token
        patch :update, {id: @room.id, room: {owner_first_name: "hemanth"} }
      end

      it "should respond with the error" do
        room_response = json_response
        expect(room_response[:success]).to eql(false)
        expect(room_response[:errors]).to eql("Insufficient Previleges for user on this room")
      end

      it {should respond_with 422}
    end

    context "When profile and room exists, but user is neither an admin, nor a room mate" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room = FactoryGirl.create :room
        @profile.rooms << FactoryGirl.create(:room)
        @profile.admin_room_id = @room.id
        @profile.save

        api_authorization_header @user.auth_token
        patch :update, {id: @room.id, room: {owner_first_name: "hemanth"} }
      end

      it "should respond with the error" do
        room_response = json_response
        expect(room_response[:success]).to eql(false)
        expect(room_response[:errors]).to eql("Insufficient Previleges for user on this room")
      end

      it {should respond_with 422}
    end

    context "When profile and room exists, but user doesn't have any rooms" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room = FactoryGirl.create :room
        @profile.admin_room_id = @room.id
        @profile.save

        api_authorization_header @user.auth_token
        patch :update, {id: @room.id, room: {owner_first_name: "hemanth"} }
      end

      it "should respond with the error" do
        room_response = json_response
        expect(room_response[:success]).to eql(false)
        expect(room_response[:errors]).to eql("User's Profile doesn't have any rooms")
      end

      it {should respond_with 422}
    end

    context "When profile and room exists, and all the required details aren't sent" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room = FactoryGirl.create :room
        @profile.rooms << @room
        @profile.admin_room_id = @room.id
        @profile.save

        api_authorization_header @user.auth_token
        patch :update, {id: @room.id, room: {owner_first_name: ""} }
      end

      it "it should return error" do
        room_response = json_response
        expect(room_response[:errors]).to have_key(:owner_first_name)
        expect(room_response[:errors][:owner_first_name]).to include "can't be blank"
      end

      it {should respond_with 422}
    end

    context "when profile doesn't exist" do
      before(:each) do
        @user = FactoryGirl.create :user
        @room = FactoryGirl.create :room
        api_authorization_header @user.auth_token
        patch :update, {id: @room.id, room: {owner_first_name: "hemanth"}}
      end

      it "should return error saying the profile doesn't exist" do
        room_response = json_response
        expect(room_response[:success]).to eql(false)
        expect(room_response[:errors]).to eql("User Profile doesn't exist. Create a profile first")
      end

      it {should respond_with 422}
    end

    context "When profile exists, but the room doesn't exist" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room = FactoryGirl.create :room
        @profile.rooms << @room
        @profile.admin_room_id = @room.id
        @profile.save

        api_authorization_header @user.auth_token
        patch :update, {id: -2, room: {owner_first_name: "hemanth"} }
      end

      it "should respond with the error" do
        room_response = json_response
        expect(room_response[:success]).to eql(false)
        expect(room_response[:errors]).to eql("Room doesn't exist")
      end

      it {should respond_with 422}
    end
  end

  describe "DELETE #destroy" do
    context "When admin is destroying the room, and admin is the only member of the room" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room = FactoryGirl.create :room
        @profile.rooms << @room
        @profile.admin_room_id = @room.id
        @profile.save

        api_authorization_header @user.auth_token
        delete :destroy, {id: @room.id}
      end

      it "should remove the association and the room" do
        expect(Room.all().count()).to eql(0)
      end

      it {should respond_with 204}
    end

    context "When admin is destroying the room, and admin is not the only member of the room" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @user1 = FactoryGirl.create :user
        @profile1 = FactoryGirl.create :person, user: @user1
        @room = FactoryGirl.create :room
        @profile.rooms << @room
        @profile.admin_room_id = @room.id
        @profile.save

        @room.persons << @profile1
        @room.save

        api_authorization_header @user.auth_token
        delete :destroy, {id: @room.id}
      end

      it "should remove the association and the room" do
        room_resp = json_response
        expect(room_resp[:success]).to eql(false)
        expect(room_resp[:errors]).to eql("Room can be deleted only if empty. Other users are in the room.")
      end

      it {should respond_with 422}
    end

    context "When user is destroying the room" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @user1 = FactoryGirl.create :user
        @profile1 = FactoryGirl.create :person, user: @user1
        @room = FactoryGirl.create :room
        @profile.rooms << @room
        @profile.admin_room_id = @room.id
        @profile.save

        @room.persons << @profile1
        @room.save

        api_authorization_header @user1.auth_token
        delete :destroy, {id: @room.id}
      end

      it "should remove the association and the room" do
        room_resp = json_response
        expect(room_resp[:success]).to eql(false)
        expect(room_resp[:errors]).to eql("Insufficient Previleges for user on this room")
      end

      it {should respond_with 422}
    end

    context "when profile doesn't exist" do
      before(:each) do
        @user = FactoryGirl.create :user
        @room = FactoryGirl.create :room
        api_authorization_header @user.auth_token
        delete :destroy, {id: @room.id}
      end

      it "should return error saying the profile doesn't exist" do
        room_response = json_response
        expect(room_response[:success]).to eql(false)
        expect(room_response[:errors]).to eql("User Profile doesn't exist. Create a profile first")
      end

      it {should respond_with 422}
    end

    context "When profile exists, but the room doesn't exist" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room = FactoryGirl.create :room
        @profile.rooms << @room
        @profile.admin_room_id = @room.id
        @profile.save

        api_authorization_header @user.auth_token
        delete :destroy, {id: -1}
      end

      it "should respond with the error" do
        room_response = json_response
        expect(room_response[:success]).to eql(false)
        expect(room_response[:errors]).to eql("Room doesn't exist")
      end

      it {should respond_with 422}
    end
  end

  describe "GET #search" do
    context "When a user submits a search query" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room1 = FactoryGirl.create :room
        @room1.room_name = 'Phi Beta Kappa'
        @room1.save
        @room2 = FactoryGirl.create :room
        @room2.room_name = 'Caesars Palace'
        @room2.save
        @room3 = FactoryGirl.create :room
        @room3.room_name = 'Terminus Cave'
        @room3.save
        api_authorization_header @user.auth_token
        get :search, {query: 'Pa'}
      end

      it "should respond with the results" do
        search_response = json_response
        expect(search_response[:success]).to eql(true)
        expect(search_response[:data][:rooms].count()).to eql(2)
      end

      it {should respond_with 200}
    end

    context "When the user doesn't submit a query" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room1 = FactoryGirl.create :room
        @room1.room_name = 'Phi Beta Kappa'
        @room1.save
        @room2 = FactoryGirl.create :room
        @room2.room_name = 'Caesars Palace'
        @room2.save
        @room3 = FactoryGirl.create :room
        @room3.room_name = 'Terminus Cave'
        @room3.save
        api_authorization_header @user.auth_token
        get :search, {query: ''}
      end

      it "should respond with the results" do
        search_response = json_response
        expect(search_response[:success]).to eql(true)
        expect(search_response[:data][:rooms].count()).to eql(3)
      end

      it {should respond_with 200}
    end

    context "When the user's room doesn't exist" do
      before(:each) do
        @user = FactoryGirl.create :user
        @profile = FactoryGirl.create :person, user: @user
        @room1 = FactoryGirl.create :room
        @room1.room_name = 'Phi Beta Kappa'
        @room1.save
        @room2 = FactoryGirl.create :room
        @room2.room_name = 'Caesars Palace'
        @room2.save
        @room3 = FactoryGirl.create :room
        @room3.room_name = 'Terminus Cave'
        @room3.save
        api_authorization_header @user.auth_token
        get :search, {query: 'abcd'}
      end

      it "should respond with the results" do
        search_response = json_response
        expect(search_response[:success]).to eql(true)
        expect(search_response[:data][:rooms].count()).to eql(0)
      end

      it {should respond_with 200}
    end
  end

end
