module ApplicationHelper
end

class DateTimeConvertor
  def self.convertRESTDateTimeToDate inputDate
    if !inputDate.is_a?(String)
      return inputDate.strftime("%m-%d-%Y")
    else
      return Time.zone.parse(inputDate).utc.strftime("%m-%d-%Y")
    end
  end

  def self.convertDbDateTimeToDate inputDate
    if !inputDate.is_a?(String)
      return inputDate.strftime("%m-%d-%Y")
    else
      return Time.zone.parse(inputDate).utc.strftime("%m-%d-%Y")
    end

  end

  def self.convertDateTimeToRailsDateTime inputDate

  end
end