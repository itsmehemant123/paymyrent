class RoomSerializer < ActiveModel::Serializer
  attributes :id, :room_name, :owner_first_name, :owner_last_name, :rent
end
