class PersonSerializer < ActiveModel::Serializer
  attributes :first_name, :last_name, :display_picture
end
