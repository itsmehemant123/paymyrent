class DailyPaymentJob
  @queue = :daily_payments_processor

  def self.perform
    payHandle = PaymentHandler.new
    #room_list = Room.all()
    #getting all rooms where pay period start is 2 months ago
    room_list = Room.where('pay_period_start > ?', Time.zone.now - 2.month)

    room_list.each do |room|

      #add check for current time period + correct money?
      transactions = CurrentTransaction.where(room_id: room.id)

      if transactions.count() == room.persons.count()
        #all roommates paid the rent - send the cash to owner, update the room for next pay period and move transactions to audit

        #call the send payment to owner - on success, the above actions are performed, else, leave them as they are
        #payHandle.send_payment_to_owner

      else

        #trigger the payment gateway for roommates who didn't pay
        room.persons.each do |person|
          if transactions.where(user_id: person.user_id).count == 0
            #payHandle.send_payment_from_user(person.user_id, )
          end
        end


      end

    end
  end

end