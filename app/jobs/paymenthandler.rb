class PaymentHandler
  require 'rest-client'
  require 'exceptions'
  require 'actionhandlers'

  def send_payment_from_user(user_id, merchant_id, merchant_key, amount, name, email, phone, info, date, mode, status)

    begin
    #add the payU rest url here
    RestClient.get('http://my-rest-service.com/resource'){ |response, request, result, &block|
      case response.code
        when 200
          #add to database?
          #send notification

          #add_record_to_current_transaction(user_id, request, response)

          #return result?
        when 423
          raise Exceptions::RestClientException.new("Errors at #{result} and #{response}")
        else
          logger.debug { "Unable to process user payment - request: #{request}, response: #{response}, result: #{result}, block: #{block}" }
          response.return!(request, result, &block)
      end
    }

    rescue Exceptions::RestClientException => exception
      logger.debug { "Unable to process user payment - exception: #{exception.message}" }
    rescue => exception
      #puts("Exception caught at PaymentHandler/send_payment, info : #{exception.message} #{exception.backtrace.join("\n")}" )
      logger.debug { "Exception caught at PaymentHandler/send_payment, info : #{exception.message} #{exception.backtrace.join("\n")}" }
    end
  end

  def send_payment_to_owner(room_id, merchant_id, merchant_key, amount, name, email, phone, info, date, mode, status)
    begin
      #add the payU rest url here
      RestClient.get('http://my-rest-service.com/resource'){ |response, request, result, &block|
        case response.code
          when 200
            #add to database?
            #send notification
            #return result?
            #move_records_to_audit_and_update_room(room_id)

          when 423
            raise Exceptions::RestClientException.new("Errors at #{result} and #{response}")
          else
            logger.debug { "Unable to process owner payment - request: #{request}, response: #{response}, result: #{result}, block: #{block}" }
            response.return!(request, result, &block)
        end
      }

    rescue Exceptions::RestClientException => exception
      logger.debug { "Unable to process owner payment - exception: #{exception.message}"}
    rescue => exception
      #puts("Exception caught at PaymentHandler/send_payment, info : #{exception.message} #{exception.backtrace.join("\n")}" )
      logger.debug { "Exception caught at PaymentHandler/send_payment_to_owner, info : #{exception.message} #{exception.backtrace.join("\n")}" }
    end
  end

  private

  def send_notification_to_user_for_bad_status(user_id, rec_props)
    user = User.where(id: user_id).first
    person = user.person
    room = person.rooms.first

    notifHandler = ActionHandlers::NotificationActionHandle.new()


    days = (Time.zone.now - room.pay_period_start).to_i / 1.day

    if days < 15 && days > 10
      notifHandler.addNotification(from: days, by: user_id, target: room.id, type: ActionHandlers::ActionTypes::INFO_ROOMRENTPENDINGPERIOD)
    elsif days < 10
      notifHandler.addNotification(from: days, by: user_id, target: room.id, type: ActionHandlers::ActionTypes::INFO_ROOMRENTPENDINGPERIOD)
      RentPendingMailer.send_reminder(user_id, room.id, days).deliver_now
    end


  end

  def add_record_to_current_transaction(user_id, sent_props, rec_props)
    user = User.where(id: user_id)
    person = user.person
    room = person.rooms.first
    amount = (room.rent/(room.persons.count))

    CurrentTransaction.create(user_id: user_id, room_id: room.id, pay_period: room.pay_period_start, amount: amount, sent_props: sent_props, received_props: rec_props)

    ActionHandlers::NotificationActionHandle.new.addNotification(from: -1, by: user_id, target: room.id, type: ActionHandlers::ActionTypes::INFO_ROOMRENTPROCESSED)
  end

  def move_records_to_audit_and_update_room(room_id)
    room = Room.where(id: room_id).first

    room.rent_left = room.rent
    if room.pay_period_type == 'Monthly'
      room.pay_period_start = room.pay_period_start + 1.month
    elsif room.pay_period_type == 'Fortnightly'
      room.pay_period_start = room.pay_period_start + 2.week
    end

    room.save

    transactions = CurrentTransaction.where(room_id: room.id)

    transactions.each do |transaction|
      TransactionsAudit.create(user_id: transaction.user_id, room_id: transaction.room_id, pay_period: transaction.pay_period, amount: transaction.amount, sent_props: transaction.sent_props, received_props: transaction.received_props)
      transaction.destroy
    end

    room.persons.each do |person|
      ActionHandlers::NotificationActionHandle.new.addNotification(from: -1, by: person.user_id, target: room.room_id, type: ActionHandlers::ActionTypes::INFO_ROOMRENTPAID)
    end
  end

end