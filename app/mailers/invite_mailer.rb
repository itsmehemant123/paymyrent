class InviteMailer < ApplicationMailer
  default from: "no-reply@paymyrent.in"

  def send_invite(invite)
    @invite = invite
    mail to: @invite.email, subject: "Invited to Paymyrent"
  end
end
