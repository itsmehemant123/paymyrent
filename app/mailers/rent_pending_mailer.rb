class RentPendingMailer < ApplicationMailer
  default from: "no-reply@paymyrent.in"

  def send_reminder(user_id, room_id, days)
    user = User.where(id: user_id).first()
    @room = Room.where(id: room_id).first()
    @days = days
    mail to: user.email, subject: "Pending Rent Payment"
  end

end
