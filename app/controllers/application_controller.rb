class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  include Authenticable

  unless Rails.application.config.consider_all_requests_local # so that in development you would see real exceptions, but not in production.
    rescue_from ActionController::RoutingError, with: -> { routing_error_handle  }
    rescue_from ActionDispatch::ParamsParser::ParseError, with: -> { json_parse_error_handle }
  end

  #for allowing ApplicationController to rescue from Routing errors
  def raise_not_found!
    logger.debug "Exception ActionController::RoutingError hit at #{params[:unmatched_route]}"
    raise ActionController::RoutingError.new("No route matches #{params[:unmatched_route]}")
  end

  def routing_error_handle
    #Log first
    logger.debug { "Routing Error caught" }
    render json: {success: false, errors: "Internal Error", code: "Critical"}, status: 404
  end

  def json_parse_error_handle
    #Log first
    logger.debug { "Json Parse Error caught" }
    render json: {success: false, errors: "Invalid payload"}, status: 400
  end

end
