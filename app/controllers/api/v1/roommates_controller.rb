class Api::V1::RoommatesController < ApplicationController
  include ::ActionController::Serialization
  before_action :authenticate_with_token!, only: [:show, :add, :remove, :join, :leave]
  before_action :user_has_profile!, only: [:show, :add, :remove, :join, :leave]
  before_action :room_exists!, only: [:show, :add, :remove, :join, :leave]
  before_action :user_is_room_admin!, only: [:add, :remove]
  respond_to :json

  require 'exceptions'
  require 'handlers'
  require 'actionhandlers'

  def show
    begin
      room = current_room
      render json: {success: true, data: {people: get_serialized_list_persons(room.persons)}}, status: 200
    rescue => exception
      logger.debug { "Exception caught at roommates/show, info: #{exception.message} #{exception.backtrace.join("\n")}" }
      render json: {success: false, errors: "Invalid room", code: "Generic"}, status: 422
    end
  end

  def add
    begin
      room = current_room
      email_id = roommate_email
      unless valid_email? email_id
        raise Exceptions::InvalidEmailException.new("Email invalid")
      end
      roommate_user = User.where(email: email_id).first()

      notifHandler = ActionHandlers::NotificationActionHandle.new()

      if roommate_user.nil?
        #invite user and send email
        invite_id = Handlers::InvitesHandler.SendInvite(email_id, room.id)
        #push notification to admin
        notifHandler.addNotification(from: invite_id, by: current_user.id, target: room.id, type: ActionHandlers::ActionTypes::INFO_ROOMJOININVITESENT)

      else
        if roommate_user.person.rooms.count() > 0
          raise Exceptions::RoomMateHasRoomException.new("This user is in another room")
        end
        room.persons << roommate_user.person
        room.save

        notifHandler.addNotification(from: current_user.id, by: roommate_user.id, target: room.id, type: ActionHandlers::ActionTypes::INFO_USERADDEDTOROOM)
      end

      render json: {success: true}, status: 201
    rescue ActionController::ParameterMissing
      render json: {success: false, errors: "Missing Email"}, status: 422
    rescue Exceptions::InvalidEmailException => exception
      render json: {success: false, errors: exception.message}, status: 422
    rescue Exceptions::RoomMateHasRoomException => exception
      render json: {success: false, errors: exception.message}, status: 422
    rescue => exception
      #puts("Exception caught at roommates/add, info: #{exception.message} #{exception.backtrace.join("\n")}")
      logger.debug { "Exception caught at roommates/add, info: #{exception.message} #{exception.backtrace.join("\n")}" }
      render json: {success: false, errors: "Invalid room", code: "Generic"}, status: 422
    end
  end

  def remove
    begin
      room = current_room
      email_id = roommate_email
      unless valid_email? email_id
        raise Exceptions::InvalidEmailException.new("Email invalid")
      end

      roommate_user = User.where(email: email_id).first()

      #check if roommate is a user
      unless roommate_user.nil?

        if roommate_user.id == current_user.id
          raise Exceptions::CannotRemoveAdminFromRoomException.new('Cannot remove self/admin from room')
        end

        #check if the room mate actually exists in the room
        if room.persons.where(id: roommate_user.person.id).count > 0
          room.persons.delete(roommate_user.person)
          room.save
          #push notification to user
          ActionHandlers::NotificationActionHandle.new().addNotification(from: current_user.id, by: roommate_user.id, target: room.id, type: ActionHandlers::ActionTypes::INFO_USERREMOVEDFROMROOM)
        end
      end

      render json: {success: true}, status: 200
    rescue ActionController::ParameterMissing
      render json: {success: false, errors: "Missing Email"}, status: 422
    rescue Exceptions::InvalidEmailException => exception
      render json: {success: false, errors: exception.message}, status: 422
    rescue Exceptions::CannotRemoveAdminFromRoomException => exception
      render json: {success: false, errors: exception.message}, status: 422
    rescue => exception
      puts("Exception caught at roommates/remove, info: #{exception.message} #{exception.backtrace.join("\n")}" )
      logger.debug { "Exception caught at roommates/remove, info: #{exception.message} #{exception.backtrace.join("\n")}" }
      render json: {success: false, errors: "Invalid room", code: "Generic"}, status: 422
    end
  end

  def join
    begin
      room = current_room
      admin = Person.where(admin_room_id: room.id).first()

      if admin.nil?
        raise Exceptions::RoomHasNoAdminException.new("This room doesn't have an admin")
      end

      ActionHandlers::NotificationActionHandle.new().addNotification(from: current_user.id, by: admin.user_id, target: room.id, type: ActionHandlers::ActionTypes::SHOULD_USERJOINROOMREQUEST)
      render json: {success: true}, status: 201
    rescue Exceptions::RoomHasNoAdminException => exception
      render json: {success: false, errors: exception.message}, status: 422
    rescue => exception
      logger.debug { "Exception caught at roommates/join, info: #{exception.message} #{exception.backtrace.join("\n")}" }
      render json: {success: false, errors: "Invalid room", code: "Generic"}, status: 422
    end
  end

  def leave
    begin
      room = current_room
      current_person = current_user.person

      if room.persons.where(id: current_person.id).count() == 0
        raise Exceptions::UserNotInRoomException.new('This user is not in the room')
      end

      if current_person.admin_room_id == room.id
        unless room.persons.count() > 1
          raise Exceptions::CannotLeaveRoomException.new('You are the only member of the room. Delete the room instead.')
        end
      end

      room.persons.delete(current_person)
      room.save

      if current_person.admin_room_id == room.id
        #make next member admin
        next_admin = room.persons.order('created_at ASC')[0]
        current_person.admin_room_id = nil
        current_person.save
        next_admin.admin_room_id = room.id
        next_admin.save
        #push notification
        ActionHandlers::NotificationActionHandle.new().addNotification(from: current_person.user_id, by: next_admin.user_id, target: room.id, type: ActionHandlers::ActionTypes::INFO_USERISROOMADMIN)
        #then continue
      end

      room.persons.map do |person|
        ActionHandlers::NotificationActionHandle.new().addNotification(from: current_person.user_id, by: person.user_id, target: room.id, type: ActionHandlers::ActionTypes::INFO_USERLEFTROOM)
      end

      render json: {success: true}, status: 201
    rescue Exceptions::CannotLeaveRoomException => exception
      render json: {success: false, errors: exception.message}, status: 422
    rescue Exceptions::UserNotInRoomException => exception
      render json: {success: false, errors: exception.message}, status: 422
    rescue => exception
      logger.debug { "Exception caught at roommates/leave, info: #{exception.message} #{exception.backtrace.join("\n")}" }
      render json: {success: false, errors: "Invalid room", code: "Generic"}, status: 422
    end
  end

  private

  def valid_email? input_mail
    valid_email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

    if input_mail =~ valid_email_regex
      return true
    end

    return false
  end

  def get_serialized_list_persons(person_list)
    #ActiveModel::ArraySerializer.new(person_list, each_serializer: PersonSerializer)
    #Above line would work if the display picture isn't required - tests fail

    #This uglyness is due to paperclip - display picture.
    #I'm simply incapable of fixing this :(
    if Rails.env.production? || Rails.env.development?
      serialized_list = person_list.map do |person|
        user = User.where(id: person.user_id).first()
        { :first_name => person.first_name, :last_name => person.last_name, :display_picture => person.display_picture, :email => user.email, :id => user.id }
      end

      return serialized_list
    else
      serialized_list = person_list.map do |person|
        user = User.where(id: person.user_id).first()
        { :first_name => person.first_name, :last_name => person.last_name, :email => user.email, :id => user.id }
      end

      return serialized_list
    end

  end

  def current_room
    begin
      room_id = params.require(:id).to_i
      Room.where(id: room_id).first
    rescue ActionDispatch::ParamsParser::ParseError
      render json: {success: false, errors: "wrong syntax"}, status: 422
    end
  end

  def roommate_email
    begin
      params.require(:email)
    rescue ActionDispatch::ParamsParser::ParseError
      render json: {success: false, errors: "wrong syntax"}, status: 422
    end
  end

  def user_has_profile!
    render json: { success: false, errors: "User Profile doesn't exist. Create a profile first" },
           status: 422 if current_user.person.nil?
  end

  def room_exists!
    render json: {success: false, errors: "Such a room doesn't exist"}, status: 422 if Room.where(id: params[:id]).count() <= 0
  end

  def user_is_room_admin!
    render json: {success: false, errors: "User cannot add room-mates to this room"}, status: 422 if (params[:id].to_i != current_user.person.admin_room_id || current_user.person.rooms.where(id: params[:id]).count() <= 0)
  end

end
