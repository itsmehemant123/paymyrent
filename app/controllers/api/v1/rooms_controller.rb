class Api::V1::RoomsController < ApplicationController
  include ::ActionController::Serialization
  before_action :authenticate_with_token!, only: [:show, :create, :update, :destroy]
  before_action :user_has_profile!, only: [:show, :create, :update, :destroy]
  before_action :user_has_rooms!, only: [:show, :update, :destroy]
  respond_to :json

  require 'actionhandlers'
  require 'exceptions'

  def show
    begin
      user = current_user
      user_room = user.person.rooms.first
      render json: {success: true, data: {email: user.email, room: serialize_single_room(user_room)}}, status: 200

    rescue => exception
      #Logging
      logger.debug { "Exception caught at rooms/show, info : #{exception.message} #{exception.backtrace.join("\n")}" }
      render json: {success: false, errors: "Invalid room", code: "Generic"}, status: 422
    end
  end

  def create
    begin
      user = current_user
      person = user.person

      if !person.admin_room_id.nil? || person.rooms.count() >= 1
        #Profile already is an Admin for a room or is a room mate
        raise Exceptions::RoomExistsInProfileException.new("Profile already has a room")
      end
      if room_already_exists? room_params
        #check for existing room
        raise Exceptions::RoomAlreadyExistsException.new("This room already exists")
      end

      room = Room.new(room_params)
      room.persons << person

      unless room.save
        raise Exceptions::RoomParamsMissingException.new(room.errors)
        #render json: {success: false, errors: room.errors}, status: 422 and return
      end

      person.admin_room_id = room.id
      person.save

      render json: {success: true, data: {room: serialize_single_room(room)} }, status: 201
    rescue Exceptions::RoomParamsMissingException => exception
      render json: {success: false, errors: exception.validation_errors}, status: 422
    rescue Exceptions::RoomExistsInProfileException => exception
      render json: {success: false, errors: exception.message}, status: 422
    rescue Exceptions::RoomAlreadyExistsException => exception
      render json: {success: false, errors: exception.message}, status: 422
    rescue => exception
      #Logging
      puts("Exception caught at rooms/create, info: #{exception.message} #{exception.backtrace.join("\n")}" )
      logger.debug { "Exception caught at rooms/create, info: #{exception.message} #{exception.backtrace.join("\n")}" }
      render json: {success: false, errors: "Invalid room", code: "Generic"}, status: 422
    end
  end

  def update
    begin
      user = current_user
      profile = user.person
      room_id = params[:id].to_i

      if Room.where(id: room_id).count() == 0
        raise Exceptions::RoomDoesntExistException.new("Room doesn't exist")
      end

      if profile.rooms.where(id: room_id).count() == 0 || profile.admin_room_id != room_id
        raise Exceptions::RoomUserNotAdminOrMateException.new("Insufficient Previleges for user on this room")
      end

      #if room already exists
      if room_params.has_key? :owner_bank_account
        dup_room = Room.where(owner_bank_account: room_params[:owner_bank_account]).where.not(id: room_id)
        if dup_room.count() > 0
          raise Exceptions::RoomAlreadyExistsException.new("This room already exists")
        end
      end
      #raise error for duplicated bank_account

      room = profile.rooms.where(id: room_id).first
      if room.update(room_params)

        #send notifications
        room.persons.map do |person|
          ActionHandlers::NotificationActionHandle.new().addNotification(from: user.id, by: person.user_id, target: room.id, type: ActionHandlers::ActionTypes::INFO_ROOMUPDATED)
        end

        render json: {success: true}, status: 200
      else
        raise Exceptions::RoomParamsMissingException.new(room.errors)
      end

    rescue Exceptions::RoomDoesntExistException => exception
      render json: {success: false, errors: exception.message}, status: 422
    rescue Exceptions::RoomParamsMissingException => exception
      render json: {success: false, errors: exception.validation_errors}, status: 422
    rescue Exceptions::RoomUserNotAdminOrMateException =>exception
      render json: {success: false, errors: exception.message}, status: 422
    rescue Exceptions::RoomAlreadyExistsException => exception
      render json: {success: false, errors: exception.message}, status: 422
    rescue => exception
      #puts("Exception caught at rooms/update, info: #{exception.message} #{exception.backtrace.join("\n")}")
      logger.debug { "Exception caught at rooms/update, info: #{exception.message} #{exception.backtrace.join("\n")}" }
      render json: {success: false, errors: "Invalid room", code: "Generic"}, status: 422
    end
  end

  def destroy
    begin
      user = current_user
      profile = user.person
      room_id = params[:id].to_i
      room_l = Room.where(id: room_id)

      if room_l.count() == 0
        raise Exceptions::RoomDoesntExistException.new("Room doesn't exist")
      end

      room = room_l.first()

      if profile.rooms.where(id: room_id).count() == 0 || profile.admin_room_id != room_id
        raise Exceptions::RoomUserNotAdminOrMateException.new("Insufficient Previleges for user on this room")
      end

      if room.persons.count() > 1 #other users in room apart from admin
        raise Exceptions::CannotDeleteRoomException.new("Room can be deleted only if empty. Other users are in the room.")
      end

      room.persons.delete_all
      room.destroy
      head 204

    rescue Exceptions::CannotDeleteRoomException => exception
      render json: {success: false, errors: exception.message}, status: 422
    rescue Exceptions::RoomDoesntExistException => exception
      render json: {success: false, errors: exception.message}, status: 422
    rescue Exceptions::RoomUserNotAdminOrMateException =>exception
      render json: {success: false, errors: exception.message}, status: 422
    rescue => exception
      #puts("Exception caught at rooms/destroy, info: #{exception.message} #{exception.backtrace.join("\n")}")
      logger.debug { "Exception caught at rooms/destroy, info: #{exception.message} #{exception.backtrace.join("\n")}" }
      render json: {success: false, errors: "Invalid room", code: "Generic"}, status: 422
    end
  end

  def search
    begin
      search_params = params[:query]
      if search_params.nil?
        raise Exceptions::NoSearchParamsException.new('No search query specified')
      end

      render json: {success: true, data: {rooms: serialize_room(Room.search(search_params).limit(25))} }, status: 200

    rescue Exceptions::NoSearchParamsException => exception
      render json: {success: false, errors: exception.message}, status: 422
    rescue => exception
      #puts("Exception caught at rooms/search, info: #{exception.message} #{exception.backtrace.join("\n")}")
      logger.debug { "Exception caught at rooms/search, info: #{exception.message} #{exception.backtrace.join("\n")}" }
      render json: {success: false, errors: "Invalid search", code: "Generic"}, status: 422
    end
  end

  private

  def serialize_room(room_list)
    #until active model serializer gem is at version 10
    room_list.map do |room|
      { :room_name => room.room_name, :owner_first_name => room.owner_first_name, :owner_last_name => room.owner_last_name, :rent => room.rent, :rent_left => room.rent_left, :pay_period_start => room.pay_period_start, :pay_period_type => room.pay_period_type, :id => room.id}
    end
  end

  def serialize_single_room(room)

    { :room_name => room.room_name, :owner_first_name => room.owner_first_name, :owner_last_name => room.owner_last_name, :rent => room.rent, :rent_left => room.rent_left, :pay_period_start => room.pay_period_start, :pay_period_type => room.pay_period_type, :id => room.id}
  end

  def room_already_exists?(input_room)
    Room.where(owner_bank_account: input_room[:owner_bank_account]).count() > 0
  end

  def user_has_profile!
    render json: { success: false, errors: "User Profile doesn't exist. Create a profile first" },
           status: 422 if current_user.person.nil?
  end

  def user_has_rooms!
    render json: {success: false, errors: "User's Profile doesn't have any rooms"},
           status: 422 if current_user.person.rooms.length == 0
  end

  def check_if_complete(profile)
    unless profile.bank_account.nil? || profile.address.nil? || profile.phone.nil?
      return true
    end
    return false
  end

  def room_params
    begin
      params.require(:room).permit(:rent, :rent_left, :room_name, :owner_first_name, :owner_last_name, :owner_bank_account, :pay_period_type, :pay_period_start)
    rescue ActionDispatch::ParamsParser::ParseError
      render json: {success: false, errors: "wrong syntax"}, status: 422
    end
  end

end
