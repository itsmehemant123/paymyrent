class Api::V1::SessionsController < ApplicationController
  respond_to :json

  def create
    begin
      user_password = params[:session][:password]
      user_email = params[:session][:email]
      user = user_email.present? && User.find_by(email: user_email)

      if !user.nil? && user.valid_password?(user_password)
        sign_in user, store: false
        user.generate_authentication_token!
        user.save
        render json: {success: true, data: {email: user.email, auth_token: user.auth_token}}, status: 200, location: [:api, user]
      else
        render json: {success: false,  errors: "Invalid email or password" }, status: 422
      end
    rescue ActionController::ParameterMissing
      logger.debug "Exception ActionController::ParameterMissing caught at sessions/create, info : email - #{params[:session][:email]}"
      render json: {success: false, errors: "no user details sent"}, status: 422
    rescue => exception
      #Logging
      logger.debug { "Exception caught at sessions/create, info : #{exception.message} #{exception.backtrace.join("\n")}" }
      render json: {success: false, errors: "Invalid email or password", code: "Generic"}, status: 422
    end

  end

  def destroy
    begin
      #if authtoken isn't existing in the db, User.find_by is null => user.generate_authentication_token! is throwing exception.
      #create another Exception class, throw and rescue it.
      user = User.find_by(auth_token: params[:id])
      user.generate_authentication_token!
      user.save
    rescue => exception
      #Logging
      logger.debug { "Exception caught at sessions/destroy, info : #{exception.message} #{exception.backtrace.join("\n")}" }
    ensure
      head 204
    end
  end

end
