class Api::V1::ProfilesController < ApplicationController
  before_action :authenticate_with_token!, only: [:show, :create, :update]
  respond_to :json

  require 'actionhandlers'

  def show
    begin
      user = current_user
      if user.person.nil?
        render json: {success: false, errors: "Profile not yet created"}, status: 422
      else
        details = profile_params_return user.person
        render json: {success: true, data: {email: user.email, profile: details}}, status: 200
      end
    rescue => exception
      #Logging
      logger.debug { "Exception caught at profiles/show, info : #{exception.message} #{exception.backtrace.join("\n")}" }
      render json: {success: false, errors: "Invalid user", code: "Generic"}, status: 422
    end
  end

  def create
    begin

      unless current_user.person.nil?
        render json: {success: false, errors: "profile already exists"}, status: 422 and return
      end

      person = current_user.build_person(profile_params)
      unless params[:profile][:image].nil?
        params[:profile][:image] = parse_image_data(params[:profile][:image]) if params[:profile][:image]
        person.display_picture = params[:profile][:image]
      end

      if person.save

        check_and_delete_invite(current_user)

        details = profile_params_return person
        render json: {success: true, data: {email: current_user.email, profile: details}}, status: 201#, location: [:api, person]
      else
        render json: {success: false, errors: person.errors}, status: 422
      end
    rescue => exception
      logger.debug { "Exception caught at profiles/create, info : #{exception.message} #{exception.backtrace.join("\n")}" }
      render json: {success: false, errors: "Invalid profile", code: "Generic"}, status: 422
    end
  ensure
    clean_tempfile
  end

  def update
    begin
      user = current_user
      profile = user.person

      if profile.update(profile_params)
        unless params[:profile][:image].nil?
          params[:profile][:image] = parse_image_data(params[:profile][:image]) if params[:profile][:image]
          profile.display_picture = params[:profile][:image]
          profile.save
        end
        details = profile_params_return profile
        render json: {success: true, data: {email: user.email, profile: details}}, status: 200#, location: [:api, profile]
      else
        render json: {success: false, errors: profile.errors}, status: 422
      end
    rescue => exception
      logger.debug { "Exception caught at profiles/update, info : #{exception.message} #{exception.backtrace.join("\n")}" }
      render json: {success: false, errors: "Invalid Profile", code: "Generic"}, status: 422
    end
  ensure
    clean_tempfile
  end

  def search
    begin
      search_params = params[:query]
      if search_params.nil?
        raise Exceptions::NoSearchParamsException.new('No search query specified')
      end

      if search_params.include?('@')
        render json: {success: true, data: {people: get_serialized_list_persons_from_users(User.search(search_params).limit(25))} }, status: 200
      else
        render json: {success: true, data: {people: get_serialized_list_persons(Person.search(search_params).limit(25))} }, status: 200
      end
    rescue Exceptions::NoSearchParamsException => exception
      render json: {success: false, errors: exception.message}, status: 422
    rescue => exception
      logger.debug { "Exception caught at profiles/search, info : #{exception.message} #{exception.backtrace.join("\n")}" }
      render json: {success: false, errors: "Invalid search", code: "Generic"}, status: 422
    end
  end

  private

  def get_serialized_list_persons_from_users(users_list)
    if Rails.env.production? || Rails.env.development?
      serialized_list = users_list.map do |user|
        person = user.person
        { :first_name => person.first_name, :last_name => person.last_name, :middle_name => person.middle_name, :display_picture => person.display_picture, :email => user.email }
      end

      return serialized_list
    else
      serialized_list = users_list.map do |user|
        person = user.person
        { :first_name => person.first_name, :last_name => person.last_name, :middle_name => person.middle_name, :email => user.email }
      end

      return serialized_list
    end
  end

  def get_serialized_list_persons(person_list)
    #ActiveModel::ArraySerializer.new(person_list, each_serializer: PersonSerializer)
    #Above line would work if the display picture isn't required - tests fail

    #This uglyness is due to paperclip - display picture.
    #I'm simply incapable of fixing this :(
    if Rails.env.production?
      serialized_list = person_list.map do |person|
        user = User.where(id: person.user_id).first()
        { :first_name => person.first_name, :last_name => person.last_name, :middle_name => person.middle_name, :display_picture => person.display_picture, :email => user.email }
      end

      return serialized_list
    else
      serialized_list = person_list.map do |person|
        user = User.where(id: person.user_id).first()
        { :first_name => person.first_name, :last_name => person.last_name, :middle_name => person.middle_name, :email => user.email }
      end

      return serialized_list
    end
  end

  def check_and_delete_invite user

    invite = Invite.where(email: user.email).first()
    unless invite.nil?
      room = Room.where(id: invite.room_id).first()
      unless room.nil?
        notifHandle = ActionHandlers::NotificationActionHandle.new()
        room.persons << user.person
        room.save

        admin = Person.where(admin_room_id: room.id).first()

        #clear the invite sent action (Sent to admin) if present
        notifHandle.load_action(by: admin.user_id, from: invite.id)
        notifHandle.clear_action()

        Invite.where(email: user.email).destroy_all

        admin = Person.where(admin_room_id: room.id).first()

        #Notification for admin of the room
        unless admin.nil?
          notifHandle.addNotification(from: user.id, by: admin.user_id, target: room.id, type: ActionHandlers::ActionTypes::INFO_INVITEUSERJOINED)
        end

      end
    end
  end

  def profile_params
    params.require(:profile).permit(:first_name, :last_name, :middle_name, :address, :phone, :bank_account, :admin_room_id, :room_id, :image)
  end

  def profile_params_return(input)
    if input.display_picture?
      {id: input.id, first_name: input.first_name, last_name: input.last_name, middle_name: input.middle_name, address: input.address, phone: input.phone, bank_account: input.bank_account, display_picture: input.display_picture.url}
    else
      {id: input.id, first_name: input.first_name, last_name: input.last_name, middle_name: input.middle_name, address: input.address, phone: input.phone, bank_account: input.bank_account, display_picture: Figaro.env.default_display_picture}
    end

  end

  # This part is actually taken from http://blag.7tonlnu.pl/blog/2014/01/22/uploading-images-to-a-rails-app-via-json-api. I tweaked it a bit by manually setting the tempfile's content type because somehow putting it in a hash during initialization didn't work for me.
  def parse_image_data(image_data)
    @tempfile = Tempfile.new('item_image')
    @tempfile.binmode
    @tempfile.write Base64.decode64(image_data[:content])
    @tempfile.rewind

    uploaded_file = ActionDispatch::Http::UploadedFile.new(
        tempfile: @tempfile,
        filename: Digest::SHA1.hexdigest(current_user.email) + ".png"
    )

    uploaded_file.content_type = image_data[:content_type]
    uploaded_file
  end

  def clean_tempfile
    if @tempfile
      @tempfile.close
      @tempfile.unlink
    end
  end

end
