class Api::V1::ActionsController < ApplicationController
  before_action :authenticate_with_token!, only: [:do_action]
  before_action :user_has_profile!, only: [:do_action]
  respond_to :json

  require 'exceptions'
  require 'handlers'
  require 'actionhandlers'

  def do_action
    begin

      action_id = params[:id]
      param_recieved = params[:verb]

      action = Action.where(id: action_id).first()

      if action.nil?
        raise Exceptions::NoActionException.new('No such action exists')
      end

      unless action.by.to_i == current_user.id.to_i
        raise Exceptions::ActionUserMismatchException.new('Logged in user cannot perform this action')
      end

      notificationHandler = ActionHandlers::NotificationActionHandle.new()
      notificationHandler.load_action_by_id(action_id)
      notificationHandler.do_and_clear_action(param_recieved)

      render json: {success: true}, status: 200

    rescue Exceptions::NoActionException => exception
      render json: {success: false, errors: exception.message}, status: 422
    rescue Exceptions::ActionUserMismatchException => exception
      render json: {success: false, errors: exception.message}, status: 422
    rescue => exception
      #puts("Exception caught at actions/do_action, info : #{exception.message} #{exception.backtrace.join("\n")}" )
      logger.debug { "Exception caught at actions/do_action, info : #{exception.message} #{exception.backtrace.join("\n")}" }
      render json: {success: false, errors: "Invalid action", code: "Generic"}, status: 422
    end
  end

  private

  def user_has_profile!
    render json: { success: false, errors: "User Profile doesn't exist. Create a profile first" },
           status: 422 if current_user.person.nil?
  end
end
