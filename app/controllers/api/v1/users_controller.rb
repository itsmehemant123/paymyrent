class Api::V1::UsersController < ApplicationController
  respond_to :json

  def show
    begin
      user = User.find(params[:id])

      if user.nil?
        render json: {success: false, errors: "user not found"}, status: 422
      else
        render json: {success: true, data: {email: user.email, id: user.id}}, status: 200, location: [:api, user]
      end
    rescue ActiveRecord::RecordNotFound
      logger.debug "Exception ActiveRecord::RecordNotFound caught at users/show, info : not found record for id - #{params[:id]}"
      render json: {success: false, errors: "user not found"}, status: 422
    rescue => exception
      #Logging
      logger.debug { "Exception caught at users/show, info : #{exception.message} #{exception.backtrace.join("\n")}" }
      render json: {success: false, errors: "Error Occured", code:"Generic"}, status: 422
    end
  end

  def create
    begin
      user = User.new(user_params)
      if user.save
        render json: {success: true, data: {email: user.email, id: user.id}}, status: 201, location: [:api, user]
      else
        render json: {success: false, errors: user.errors}, status: 422
      end
    rescue ActionController::ParameterMissing => exception
      logger.debug "Exception ActionController::ParameterMissing caught at users/create, info : #{exception}"
      render json: {success: false, errors: "no user details sent"}, status: 422
    rescue => exception
      #Logging
      logger.debug { "Exception caught at users/create, info : #{exception.message} #{exception.backtrace.join("\n")}" }
      render json: {success: false, errors: "Error Occured", code:"Generic"}, status: 422
    end

  end

  def update
    begin
      user = current_user

      if user.update(user_params)
        render json: {success: true, data: {email: user.email, id: user.id}}, status: 200, location: [:api, user]
      else
        render json: {success: false, errors: user.errors}, status: 422
      end
    rescue ActionController::ParameterMissing => exception
      logger.debug "Exception ActionController::ParameterMissing caught at users/update, info : #{exception}"
      render json: {success: false, errors: "no user details send"}, status: 422
    rescue => exception
      #Logging
      logger.debug { "Exception caught at users/update, info : #{exception.message} #{exception.backtrace.join("\n")}" }
      render json: {success: false, errors: "Error Occured", code:"Generic"}, status: 422
    end
  end

  def destroy
    current_user.destroy
    head 204
  end

  private

  def user_params
    begin
      params.require(:user).permit(:email, :password, :password_confirmation)
    rescue ActionDispatch::ParamsParser::ParseError
      render json: {success: false, errors: "wrong syntax"}, status: 422
    end
  end

end
