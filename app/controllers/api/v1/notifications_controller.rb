class Api::V1::NotificationsController < ApplicationController
  include ::ActionController::Serialization
  before_action :authenticate_with_token!, only: [:show]
  before_action :user_has_profile!, only: [:show]
  respond_to :json

  require 'exceptions'
  require 'handlers'
  require 'actionhandlers'

  def show
    begin
      user = current_user
      notifHandler = ActionHandlers::NotificationActionHandle.new()
      notifHandler.load_action(by: user.id)
      notifs = notifHandler.get_action!
      render json: {success: true, data: {notifications: notifs}}, status: 200
    rescue => exception
      logger.debug { "Exception caught at notifications/show, info: #{exception.message} #{exception.backtrace.join("\n")}" }
      render json: {success: false, errors: "Invalid profile", code: "Generic"}, status: 422

    end
  end

  private

  def user_has_profile!
    render json: { success: false, errors: "User Profile doesn't exist. Create a profile first" },
           status: 422 if current_user.person.nil?
  end

end
