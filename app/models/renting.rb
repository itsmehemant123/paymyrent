class Renting < ActiveRecord::Base
  belongs_to :room, inverse_of: :rentings
  belongs_to :person, inverse_of: :rentings
end
