class TransactionsAudit < ActiveRecord::Base
  validates :user_id, :room_id, :pay_period, :amount, presence: true
end
