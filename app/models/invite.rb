class Invite < ActiveRecord::Base
  validates :room_id, :email, presence: true
end
