class Room < ActiveRecord::Base
  #belongs_to :profile

  validates :rent, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :rent_left, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :room_name, presence: true
  validates :owner_first_name, presence: true
  validates :owner_last_name, presence: true
  validates :owner_bank_account, presence: true
  validates :pay_period_type, presence: true
  validates :pay_period_start, presence: true

  validate :rent_value

  has_many :rentings
  has_many :persons, through: :rentings

  def rent_value
    unless self.rent && self.rent_left && (self.rent >= self.rent_left)
      self.errors.add(:rent, "rent should be greater or equal to the rent left")
    end
  end

  #scope :search, lambda (search) {where("lower(room_name) like ?", "#{search.downcase}%")}
  scope :search , lambda { |keyword|
                 where("lower(room_name) LIKE ?", "%#{keyword.downcase}%" )
                }
end
