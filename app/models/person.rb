class Person < ActiveRecord::Base
  validates :first_name, :last_name, :user_id, presence: true

  #Required for the facebook/3rd party profile integrations - they may/may not be available
  #validates :address, :phone, :bank_account, presence: true

  has_attached_file :display_picture, :styles => { :thumb => ["100x100#", :png] }, :path => Figaro.env.display_picture_storage_location, :url => Figaro.env.display_picture_location, :default_url => Figaro.env.default_display_picture
  validates_attachment :display_picture, :size => { :in => 0..100.kilobytes }
  validates_attachment_content_type :display_picture, :content_type => /\Aimage\/.*\Z/

  has_many :rentings
  has_many :rooms, through: :rentings

  belongs_to :user

  scope :search , lambda { |keyword|
                  where("lower(first_name) LIKE ? OR lower(last_name) LIKE ? OR lower(middle_name) LIKE ?", "%#{keyword.downcase}%", "%#{keyword.downcase}%", "%#{keyword.downcase}%" )
                }
end
