class Action < ActiveRecord::Base
  validates :from, :action, :target, :by, presence: true
end
